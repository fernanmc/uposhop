<?php
session_start();
ob_start();

   $idUser=$_SESSION['email'];

// Rutas donde tendremos la libreria y el fichero de idiomas.
require_once('./tcpdf/tcpdf.php');
class MYPDF extends TCPDF {

//Page header
    public function Header() {
// Logo
       // $image_file = 'image_demo.jpg';
       // $this->Image($image_file, 10, 10, 15, '', 'JPG', '', 'T', false, 150, '', false, false, 0, false, false, false);
// Set font
        $this->SetFont('helvetica', 'B', 20);
// Title
        $this->Cell(0, 15, ' UPOSHOP', 0, false, 'C', 0, '', 0, false, 'M', 'M');
    }

// Page footer
    public function Footer() {
// Position at 15 mm from bottom
        $this->SetY(-15);
// Set font
        $this->SetFont('helvetica', 'I', 8);
// Page number
        $this->Cell(0, 10, 'Página ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }

}
// Crear el documento
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// Información referente al PDF
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('UPOSHOP');
$pdf->SetTitle('FacturaUpshop');
$pdf->SetSubject('Factura del pedido');
$pdf->SetKeywords('TCPDF, PDF, palabras, claves');
//Aplicamos operaciones de modificación
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
// Fuente de la cabecera y el pie de página
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
// Márgenes
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
// Saltos de página automáticos.
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
// Establecer el ratio para las imagenes que se puedan utilizar
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
// Establecer la fuente
$pdf->SetFont('times', 'BI', 10);
// Añadir página
$pdf->AddPage();
       
       include './modelos/productos.php';
       include './modelos/compras.php';
        
      if(isset($_GET['idCompra'])){
            $idCompra=$_GET['idCompra'];
            $compra=  consultarCompra($idCompra);
            $email=$idUser;
            $direccion=''.$compra['direccion'].' /'.$compra['ciudad'].' -CP: '.$compra['cp'];
            $precio=$compra['precioTotal'];
            $estado=$compra['estado'];
            $telefono=$compra['telefono'];
            $registro=$compra['fechaRegistro'];
            $modificacion=$compra['fechaActualizacion'];
        }
       

$cadena='<table  cellspacing="0" cellpadding="1" border="1">';
 $cadena.="   <thead>
        <tr>
            <td scope='col'>Email</td>
            <td scope='col'>Telefono</td>
            <td scope='col'>Dirección</td>

        </tr>
    </thead>
    <tbody>

        <tr>

            <td scope='col'> $email  </td>
            <td scope='col'> $telefono  </td>
             <td scope='col'> $direccion  </td>
        </tr>
    </tbody>
</table>
 <br>
    <h2> Productos</h2>
    <br>";
$cadena.='<table cellspacing="0" cellpadding="1" border="1">';
$cadena.="<thead>
        <tr>
            <th scope='col'>Producto</th>
            <th scope='col'>Precio</th>
            <th scope='col'>Cantidad</th>
            <th scope='col'>Total</th>
        </tr>
    </thead>
    <tbody>";
        $productosComprados = consultarProductosCompras($idCompra);
        if (count($productosComprados)) {
            foreach ($productosComprados as $productoComprado) {
                if (isset($productoComprado['idProducto'])) {

                    //$imagen = listarFoto($productoComprado['idProducto'])
                  
                    $cadena.="<tr>
                       

                        <td>
                             ".$productoComprado['nombre']." 
                        </td>

                        <td >
                             ".$productoComprado['precio'] . " €
                        </td>

                        <td>
                             ".$productoComprado['cantidad']." 

                        </td>

                        <td>
                             ".$productoComprado['cantidad'] * $productoComprado['precio'] . " €
                        </td>
                    </tr>";
                }
            }
        }
        

   $cadena.=" </tbody>
    </table>
    <br>
    <h2> Total</h2>
    <br>
    <table border='2'>
        <tbody>

            <tr>
                <th scope='col'>Importe Total</th>
                <td><strong> $precio € </strong> </td>
            </tr>
            <tr>
                <th scope='col'>Fecha Compra</th>
                <td>". $compra['fechaRegistro']." </td>
            </tr>
            <tr>
                <th scope='col'>Fecha Última Actualización</th>
                <td>". $compra['fechaActualizacion']." </td>
            </tr>
            <tr>
                <th scope='col'>Modificación</th>
                <td><strong> $estado </strong> </td>
            </tr>
        </tbody>
    </table>";
$pdf->writeHTML($cadena, true, false, true, false, '');
$pdf->Output("factura$idCompra"."-"."$idUser.pdf", 'D');