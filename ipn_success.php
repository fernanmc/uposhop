<!DOCTYPE html>
<html lang="es">
<?php
session_start();
ob_start();
if(isset($_SESSION["logueado"])){
   $idUser=$_SESSION["email"];
}else{
     session_abort();
     header("Location: login.php");
}

?>
    <head>
        <meta charset="UTF-8">
        <title>UPOShop</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.css">
    </head>
    <body>
       <?php
       
        
       include './cabecera.php';
       include './modelos/productos.php';
       include './modelos/compras.php';
        
      if(isset($_SESSION["nombre"]) && isset($_SESSION["apellidos"]) && isset($_SESSION["ciudad"])&& isset($_SESSION["cp"]) && isset($_SESSION["provincia"]) && isset($_SESSION["direccion"])&& isset($_SESSION["telefono"])){
           $nombre=$_SESSION["nombre"];
           $apellidos=$_SESSION["apellidos"];
           $ciudad=$_SESSION["ciudad"];
           $provincia=$_SESSION["provincia"];
           $direccion=$_SESSION["direccion"];
           $telefono=$_SESSION["telefono"];
           $cp=$_SESSION["cp"];
          // print_r($_SESSION);
          // print_r($_POST);
           $email=$_SESSION["email"];
           $idPago=$_POST["txn_id"];
           $precio=$_POST["transaction_subject"];
           $productos=$_COOKIE["carrito"];
           $insertado=insertarCompra($nombre,$apellidos,$ciudad,$direccion,$telefono,$precio,$cp,$email,$idPago,$productos);
           if($insertado){
               setcookie("carrito","",time() - (60*60*24));
           }
       }else{
           header("Location: cart.php");
       }
      

       ?>
        <div class="pagina-producto">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="producto-sidebar">
                        <h2 class="sidebar-titulo">Buscar Productos</h2>
                        <form method="POST" action="#">
                            <input type="text" placeholder="Buscar Productos..." name="busqueda" id="busqueda">
                            <input type="submit" value="Search" name="search">
                        </form>
                    </div>

                         <div class="producto-sidebar">
                        <h2 class="sidebar-titulo"> PRODUCTOS</h2>
                        <?php
                          $productosRelacionados= consultarProductosRecientes();
                          foreach ($productosRelacionados as $relacionado) {
                               if(isset($relacionado["idProducto"])){
                               $fotorelacionado=  listarFoto($relacionado['idProducto']);
                              
                        ?>
                        <div class="miniatura-reciente">
                            <img src="img/<?php echo $fotorelacionado[0];?>" class="miniatura" alt="">
                            <h2><a href="product.php?idProduct=<?php echo $relacionado['idProducto'] ?>"><?php echo $relacionado["nombre"];?></a></h2>
                            <div class="producto-sidebar-precio">
                                <ins><?php echo $relacionado["precio"]." €";?></ins>
                            </div>                             
                        </div>
                        <?php
                               }
                        }
                        ?>
                       
                    </div>

                        <div class="producto-sidebar">
                        <h2 class="sidebar-titulo">ÚLTIMOS PRODUCTOS</h2>
                        <ul>
                              <?php
                          $productosRecientes= consultarProductosRecientes();
                          foreach ($productosRecientes as $reciente) {
                              if(isset($reciente["idProducto"])){
                        ?>
                            <li><a href="product.php?idProduct=<?php echo $reciente['idProducto'] ?>"><?php echo  $reciente["nombre"]."-2016" ?></a></li>
                            
                              <?php
                              }
                        }
                        ?>
                        </ul>
                    </div>
                </div>
                  
                    <div class="col-md-8">
                    <div class="product-content-right">
                         <div class="col-md-6">
                   
                        <div class="alert alert-success" role="alert">
                            <ul>
                                
                                    <li>LA COMPRA SE HA REALIZADO CON EXITO</li>
                                    
                            </ul>
                            </div>
                   
                        
                    </div>
                        <input type="button"  value="Continuar" onclick="window.location.href = './index.php';" name="continuar" >
                        <div class="form-group">
                            <div class="total_carro ">
                              <h2>Datos Del Usuario</h2>

                                <table cellspacing="0">
                                    <thead>
                                        <tr>
                                            <td>Email</td>
                                            <td>Dirección</td>
                                            <td>Telefono</td>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                       
                                        <tr>
                                            
                                            <td><?php echo $_SESSION["email"] ?> </td>
                                            <td><?php echo $_SESSION["direccion"]." - ".$_SESSION["cp"]."-".$_SESSION["ciudad"]."" ?> </td>
                                            <td><?php echo $_SESSION["telefono"] ?> </td>
                                        </tr>
                                    </tbody>
                                </table>
                             </div>
                                <table cellspacing="0" class="tabla_carro cart">
                                    <thead>
                                        <tr>
                                            <th class="producto-miniatura">&nbsp;</th>
                                            <th class="producto-nombre">Product</th>
                                            <th class="producto-precio">Precio</th>
                                            <th class="producto-cantidad">Cantidad</th>
                                            <th class="product-subtotal">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                       // var_dump($_COOKIE["carrito"]);
                                        $products="";
                                        if(isset($_COOKIE["carrito"])){
                                        foreach ($_COOKIE["carrito"] as $productoCarro) {
                                                                   
                                          //  var_dump($productoCarro)   
                                            $producto=  consultarProducto($productoCarro["id"]);
                                            $imagen= listarFoto($productoCarro["id"]);
                                            $productoCarro["cantidad"]=$_COOKIE["carrito"][$productoCarro["id"]]["cantidad"];
                                            //$products.=$producto["nombre"]." Cant-".$productoCarro["cantidad"]." \n";
                                           // echo $products;
                                        ?>
                                        <tr>
                                            

                                            <td class="producto-miniatura">
                                                <a href="product.php?idProduct=<?php echo $producto["idProducto"]; ?>"><img width="145" height="145" alt="poster_1_up" class="shop_thumbnail" src="img/<?php echo $imagen[0] ?>"></a>
                                            </td>

                                            <td class="producto-nombre">
                                                <a href="product.php?idProduct=<?php echo $producto["idProducto"]; ?>"><?php echo $producto["nombre"]; ?></a> 
                                            </td>

                                            <td class="producto-precio">
                                                <span class="precio"><?php echo $producto["precio"]." €"; ?></span> 
                                            </td>

                                            <td class="producto-cantidad">
                                                <div class="cantidad">
                                                    <input type="number" readonly  class="cantidad"  title="Qty" value="<?php echo $productoCarro["cantidad"]; ?>" min="0" step="1">
                                                
                                                </div>
                                            </td>

                                            <td class="product-subtotal">
                                                <span class="precio"><?php echo $productoCarro["cantidad"]*$productoCarro["precio"]." €"; ?></span> 
                                            </td>
                                        </tr>
                                         <?php
                                           }
                                         }
                                        ?>
                                        
                                       
                                    </tbody>
                                </table>
                            </div>
                            <div class="total_carro ">
                              <h2>Precio total</h2>

                                <table cellspacing="0">
                                    <tbody>
                                       
                                        <tr>
                                            <th>Order Total</th>
                                            <td><strong><span class="precio"><?php echo $preciototal." €"; ?></span></strong> </td>
                                        </tr>
                                    </tbody>
                                </table>
                              <?php
                              foreach ($_COOKIE["carrito"] as $productoCarro) {
                                  if(isset($productoCarro["id"])){
                                    $id = $productoCarro["id"];
                                    $borrar = $productoCarro["id"];
                                    if (isset($borrar)) {
                                        setcookie("carrito[$borrar][id]", "", time() - (60 * 60 * 24));
                                        setcookie("carrito[$borrar][cantidad]", "", time() - (60 * 60 * 24));
                                        setcookie("carrito[$borrar][precio]", "", time() - (60 * 60 * 24));
                                    }
                                  }
                                }
                                ?>
                             </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
        <!--Pie de pagina footer-->
        <!--Fin del contenedor-->
        <?php
       include './pie.php';
       ?>
        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.js"></script>

    </body>
</html>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_end_flush();