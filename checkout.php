<!DOCTYPE html>
<html lang="es">
<?php
session_start();
ob_start();
if(isset($_SESSION["logueado"])){
   $idUser=$_SESSION["email"];
}else{
     session_destroy();
     header("Location: login.php");
}

?>
    <head>
        <meta charset="UTF-8">
        <title>UPOShop</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.css">
    </head>
    <body>
       <?php
       if(isset($_GET["error"])){
           if($_GET["error"]==True){
               $errores[]="LA COMPRA NO SE HA REALIZADO CORRECTAMENTE";
           }
       }
       if(isset($_SESSION["nombre"]) && isset($_SESSION["apellidos"]) && isset($_SESSION["ciudad"])&& isset($_SESSION["cp"]) && isset($_SESSION["provincia"]) && isset($_SESSION["direccion"])&& isset($_SESSION["telefono"])){
           
       }else{
           header("Location: cart.php");
       }
      
        
       include './cabecera.php';
       include './modelos/productos.php';
        
      

       ?>
        <div class="pagina-producto">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="producto-sidebar">
                        <h2 class="sidebar-titulo">Buscar Productos</h2>
                        <form method="POST" action="#">
                            <input type="text" placeholder="Buscar Productos..." name="busqueda" id="busqueda">
                            <input type="submit" value="Search" name="search">
                        </form>
                    </div>

                         <div class="producto-sidebar">
                        <h2 class="sidebar-titulo"> PRODUCTOS</h2>
                        <?php
                          $productosRelacionados= consultarProductosRecientes();
                          foreach ($productosRelacionados as $relacionado) {
                               if(isset($relacionado["idProducto"])){
                               $fotorelacionado=  listarFoto($relacionado['idProducto']);
                              
                        ?>
                        <div class="miniatura-reciente">
                            <img src="img/<?php echo $fotorelacionado[0];?>" class="miniatura" alt="">
                            <h2><a href="product.php?idProduct=<?php echo $relacionado['idProducto'] ?>"><?php echo $relacionado["nombre"];?></a></h2>
                            <div class="producto-sidebar-precio">
                                <ins><?php echo $relacionado["precio"]." €";?></ins>
                            </div>                             
                        </div>
                        <?php
                               }
                        }
                        ?>
                       
                    </div>

                        <div class="producto-sidebar">
                        <h2 class="sidebar-titulo">ÚLTIMOS PRODUCTOS</h2>
                        <ul>
                              <?php
                          $productosRecientes= consultarProductosRecientes();
                          foreach ($productosRecientes as $reciente) {
                              if(isset($reciente["idProducto"])){
                        ?>
                            <li><a href="product.php?idProduct=<?php echo $reciente['idProducto'] ?>"><?php echo  $reciente["nombre"]."-2016" ?></a></li>
                            
                              <?php
                              }
                        }
                        ?>
                        </ul>
                    </div>
                </div>
                  
                    <div class="col-md-8">
                    <div class="product-content-right">
                        <?php
                        if (isset($errores)) {
                            ?>
                            <div class="alert alert-danger" role="alert">
                                <ul>
                                    <?php
                                    foreach ($errores as $error) {
                                        ?>
                                        <li><?php echo $error; ?></li>
                                        <?php
                                    }
                                    unset($errores);
                                    ?>
                                </ul>
                            </div>
                        <?php } ?>
                        <div class="form-group">
                            <div class="total_carro ">
                              <h2>Datos Del Usuario</h2>

                                <table cellspacing="0" class="tabla_carro cart">
                                    <thead>
                                        <tr>
                                            <td>Email</td>
                                            <td>Dirección</td>
                                            <td>Telefono</td>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                       
                                        <tr>
                                            
                                            <td><?php echo $_SESSION["email"] ?> </td>
                                            <td><?php echo $_SESSION["direccion"]." - ".$_SESSION["cp"]."-".$_SESSION["ciudad"]."" ?> </td>
                                            <td><?php echo $_SESSION["telefono"] ?> </td>
                                        </tr>
                                    </tbody>
                                </table>
                             </div>
                            <form method="post" action="https:/www.sandbox.paypal.com/cgi-bin/webscr">
                                <table cellspacing="0" class="tabla_carro cart">
                                    <thead>
                                        <tr>
                                            <th class="producto-miniatura">&nbsp;</th>
                                            <th class="producto-nombre">Producto</th>
                                            <th class="producto-precio">Precio</th>
                                            <th class="producto-cantidad">Cantidad</th>
                                            <th class="product-subtotal">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $products="";
                                        if(isset($_COOKIE["carrito"])){
                                        foreach ($_COOKIE["carrito"] as $productoCarro) {
                                                                   
                                            $producto=  consultarProducto($productoCarro["id"]);
                                            $imagen= listarFoto($productoCarro["id"]);
                                            $productoCarro["cantidad"]=$_COOKIE["carrito"][$productoCarro["id"]]["cantidad"];
                                            $products.=$producto["nombre"]." Cant-".$productoCarro["cantidad"]." \n";
                                        ?>
                                        <tr>
                                            

                                            <td class="producto-miniatura">
                                                <a href="product.php?idProduct=<?php echo $producto["idProducto"]; ?>"><img width="145" height="145" alt="poster_1_up" class="shop_thumbnail" src="img/<?php echo $imagen[0] ?>"></a>
                                            </td>

                                            <td class="producto-nombre">
                                                <a href="product.php?idProduct=<?php echo $producto["idProducto"]; ?>"><?php echo $producto["nombre"]; ?></a> 
                                            </td>

                                            <td class="producto-precio">
                                                <span class="precio"><?php echo $producto["precio"]." €"; ?></span> 
                                            </td>

                                            <td class="producto-cantidad">
                                                <div class="cantidad">
                                                    <input type="number" readonly  class="cantidad"  title="Qty" value="<?php echo $productoCarro["cantidad"]; ?>" min="0" step="1">
                                                    <input type="hidden" name="shipping" value="0"> 
                                                    <input type="hidden" name="cbt" value="Presione aquí para volver a www.uposhop.com"> 
                                                    <input type="hidden" name="cmd" value="_xclick"> 
                                                    <input type="hidden" name="rm" value="2"> 
                                                    <input type="hidden" name="bn" value="UPOSHOP"> 
                                                    <input type="hidden" name="business" value="fernandomasero-facilitator-1@gmail.com"> 
                                                    <input type="hidden" name="item_name" value="<?php echo $products; ?>"> 
                                                    <input type="hidden" name="item_number" value="<?php echo $cantidadTotal ?>"> 
                                                    <input type="hidden" name="amount" value="<?php echo number_format($preciototal,2) ?>"> 
                                                    <input type="hidden" name="custom" value="<?php echo $preciototal ?>"> 
                                                    <input type="hidden" name="currency_code" value="EUR"> 
                                                    <input type="hidden" name="return" value="http://env-3084058.jelastic.dogado.eu/ipn_success.php"> 
                                                    <input type="hidden" name="cancel_return" value="http://env-3084058.jelastic.dogado.eu/checkout.php?error=true"> 
                                                    <input type="hidden" name="no_shipping" value="0"> 
                                                    <input type="hidden" name="no_note" value="0"> 
                                                    <input type='hidden' name='name' value='<?php echo $_SESSION["name"] ?>'>
                                                    <input type='hidden' name='last_name' value='<?php echo $_SESSION["apellidos"] ?>'>
                                                    <input type='hidden' name='address1' value='<?php echo $_SESSION["direccion"] ?>'>
                                                    <input type='hidden' name='city' value='<?php echo $_SESSION["ciudad"] ?>'>
                                                    <input type='hidden' name='zip' value='<?php echo $_SESSION["cp"] ?>'>
                                                    <input type='hidden' name='night_phone_a' value='<?php echo $_SESSION["telefono"] ?>'>
                                                    <input type='hidden' name='lc' value='ES'>
                                                    <input type='hidden' name='country' value='ES '>
                                                </div>
                                            </td>

                                            <td class="product-subtotal">
                                                <span class="precio"><?php echo $productoCarro["cantidad"]*$productoCarro["precio"]." €"; ?></span> 
                                            </td>
                                        </tr>
                                         <?php
                                           }
                                         }
                                        ?>
                                        <tr>
                                            <td class="acciones" colspan="6">
                                                
                                                <input type="button"  value="Volver" onclick="window.location.href = './precheckout.php';" name="Volver" >
                                                <input type="submit" value="Confirmar" name="Proceder Pago">
                                            </td>
                                        </tr>
                                       
                                    </tbody>
                                </table>
                            </div>
                            
                            </form>
                            <div class="total_carro ">
                              <h2>Precio total</h2>

                                <table cellspacing="0">
                                    <tbody>
                                       
                                        <tr>
                                            <th>Total</th>
                                            <td><strong><span class="precio"><?php echo $preciototal." €"; ?></span></strong> </td>
                                        </tr>
                                    </tbody>
                                </table>
                             </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
        <!--Pie de pagina footer-->
        <!--Fin del contenedor-->
        <?php
       include './pie.php';
       ?>
        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.js"></script>

    </body>
</html>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_end_flush();