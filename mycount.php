<!DOCTYPE html>
<html lang="es">
<?php
session_start();
ob_start();
if(isset($_SESSION["logueado"])){
   $idUser=$_SESSION["email"];
}else{
    session_destroy();
     header("Location: login.php");
}

?>
    <head>
        <meta charset="UTF-8">
        <title>UPOShop</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.css">
    </head>
    <body>
       <?php      
        
       include './cabecera.php';
       include './modelos/productos.php'; 
       include './modelos/usuarios.php';
        $usuario = consultarUser($idUser);
        $email = $usuario["email"];
        $passwordCryp = $usuario["password"];
        $password = null;
        $nombre = $usuario["nombre"];
        $apellidos = $usuario["apellidos"];
        $direccion = $usuario["direccion"];
        $telefono = $usuario["telefono"];
        $localidad = $usuario["ciudad"];
        $provincia = $usuario["provincia"];
        $cp = $usuario["cp"];
       if (isset($_POST['guardar'])) {
           $password=null;
            $filtros = Array(
                
                'email'=> FILTER_SANITIZE_MAGIC_QUOTES,
                'password'=>FILTER_SANITIZE_MAGIC_QUOTES,
                'inputPassword'=>FILTER_SANITIZE_MAGIC_QUOTES,
                'repeatPassword'=>FILTER_SANITIZE_MAGIC_QUOTES,
                'nombre' => FILTER_SANITIZE_MAGIC_QUOTES,
                'apellidos' => FILTER_SANITIZE_MAGIC_QUOTES,
                'direccion' => FILTER_SANITIZE_MAGIC_QUOTES,
                'localidad' => FILTER_SANITIZE_MAGIC_QUOTES,
                'provincia' => FILTER_SANITIZE_MAGIC_QUOTES,
                'cp' => FILTER_SANITIZE_MAGIC_QUOTES,
                'telefono' => FILTER_SANITIZE_MAGIC_QUOTES,
            );
          //  print_r($filtros);
            $result = filter_input_array(INPUT_POST, $filtros);
            if($result['email']!="" ){
                $email=$result['email'];
            }else{
                $errores[]="El email es un campo obligatorio";
            }
            if($result['password']!=""){
                $password=$result['password'];
                if(existeUsuario($idUser, $password)==True ){
                if($result["inputPassword"]== $result["repeatPassword"]){
                    if(validar_clave($result["inputPassword"])){
                        $password=$result["inputPassword"];
                    }else{
                         $errores[]="La nueva password no Cumple Con las características";
                    }
                    
                }else{
                    $errores[]="La nueva password  y la de verificación no son correctas";
                }
            }else{
                $errores[]="La password no es correcta";
            }
                
            }
            
            if($result['nombre']!=""){
                $nombre=$result['nombre'];
            }
            if($result['apellidos']!=""){
              $apellidos=$result['apellidos'];
            }
            if($result['telefono']!=null){
               $telefono=$result['telefono'];
            }
            if($result['direccion']!=""){
                //$_SESSION["direccion"] = $result['direccion'];
                $direccion=$result['direccion'];
            }
            if($result['localidad']!="Seleccione antes una localidad"){
                //$_SESSION["ciudad"] = $result['localidad'];
                $localidad=$result["localidad"];
            } 
            if($result['cp']!=""){
              if(cp_valido($result['cp'])){
                 $cp=$result["cp"];
                
            }else{
                $errores[]="El Código Posta esta vacio";
            }
            }
            if($result['telefono']!=""){
              if(telefono_valido( $result['telefono'])){
                // $_SESSION["telefono"] = $result['telefono'];
                 $telefono=$result['telefono'];
              }else{
                  $errores[]="El Teléfono no es correcto";
              }   
            }
            
            if($result['provincia']!="Seleccione su provincia..."){
                //$_SESSION["provincia"] = $result['provincia'];
                $provincia=$result['provincia'];
            }
            if(!isset($errores)){
                $actualizado=  editarUser($idUser,$email, $password, $nombre, $apellidos, $direccion, $localidad, $cp, $telefono, $provincia);
            }
        }
       ?>
        <div class="pagina-producto">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="producto-sidebar">
                        <h2 class="sidebar-titulo">Buscar Productos</h2>
                        <form method="POST" action="#">
                            <input type="text" placeholder="Buscar Productos..." name="busqueda" id="busqueda">
                            <input type="submit" value="Search" name="search">
                        </form>
                    </div>

                         <div class="producto-sidebar">
                        <h2 class="sidebar-titulo"> PRODUCTOS</h2>
                        <?php
                          $productosRelacionados= consultarProductosRecientes();
                          foreach ($productosRelacionados as $relacionado) {
                               if(isset($relacionado["idProducto"])){
                               $fotorelacionado=  listarFoto($relacionado['idProducto']);
                              
                        ?>
                        <div class="miniatura-reciente">
                            <img src="img/<?php echo $fotorelacionado[0];?>" class="miniatura" alt="">
                            <h2><a href="product.php?idProduct=<?php echo $relacionado['idProducto'] ?>"><?php echo $relacionado["nombre"];?></a></h2>
                            <div class="producto-sidebar-precio">
                                <ins><?php echo $relacionado["precio"]." €";?></ins>
                            </div>                             
                        </div>
                        <?php
                               }
                        }
                        ?>
                       
                    </div>

                        <div class="producto-sidebar">
                        <h2 class="sidebar-titulo">ÚLTIMOS PRODUCTOS</h2>
                        <ul>
                              <?php
                          $productosRecientes= consultarProductosRecientes();
                          foreach ($productosRecientes as $reciente) {
                              if(isset($reciente["idProducto"])){
                        ?>
                            <li><a href="product.php?idProduct=<?php echo $reciente['idProducto'] ?>"><?php echo  $reciente["nombre"]."-2016" ?></a></li>
                            
                              <?php
                              }
                        }
                        ?>
                        </ul>
                    </div>
                </div>
                  
                    <div class="col-md-6">
                    <div class="product-content-right">
                        <form method="post" action="">
                            <div class="form-group">
                              <?php
                                if (isset($errores)) {
                                    ?>
                                    <div class="alert alert-danger" role="alert">
                                        <ul>
                                            <?php
                                            foreach ($errores as $error) {
                                                ?>
                                                <li><?php echo $error; ?></li>
                                                <?php
                                            }
                                            unset($errores);
                                            ?>
                                        </ul>
                                    </div>
                                    <?php
                                }
                                ?>
                                
                             <h2 class="sidebar-titulo">Formulario de Usuario</h2>
                                    
                                    <?php 
                                  
                                            
                                           
                                        
                                        
                                    ?>
                                        <div class="form-group">
                                            <label>Datos del Perfil</label>
                                            <input class="form-control" type="email" name="email" id="email" placeholder="Email" onchange="checkEmail(this)" value="<?php if(isset($email))echo $email ?>">
                                            <label id="emailError" name="emailError" value=""></label>
                                            <br>
                                            <label>Cambiar Contraseña</label>
                                            <input class="form-control" type="password" name="password" id="password" placeholder="Introduzca su actual password" >
                                            <br>
                                            <label for="inputPassword" class="sr-only">Password</label>
                                            <input type="password" id="inputPassword" name="inputPassword" class="form-control" placeholder="Introduzca su nueva password" data-indicator="pwindicator"  onkeypress="fortaleza()" onchange="checkPassword(this)">
                                            <label id="passwordError" name="emailError" value=""></label>
                                            <meter id="progressbar" value="0" max="100"></meter>

                                            <label for="repeatPassword" class="sr-only">Repita Password</label>
                                            <input type="password" id="repeatPassword" name="repeatPassword" class="form-control" placeholder="Repita su nueva password"  onchange="checkConfirmPassword(this)">
                                            <label id="repeatPasswordError" name="emailError"></label>
                                            
                                        </div>
                                        <div class="form-group">
                                            <label>Nombre y Apellidos</label>
                                            <input class="form-control" type="text" name="nombre" id="nombre" placeholder="Nombre" value="<?php if(isset($nombre))echo $nombre ?>">
                                            <br>
                                            <input class="form-control" type="text" name="apellidos" id="apellidos" placeholder="Apellidos" value='<?php if(isset($apellidos)) echo $apellidos ?>'>
                                            <br>
                                            <input class="form-control" type="tel" minlength="9" maxlength="9" name="telefono" id="telefono" placeholder="NºTelefono" value="<?php if(isset($telefono))echo $telefono ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Dirección</label>
                                            
                                            <br>
                                            <input type="text" class="form-control" id="direccion" name="direccion"  placeholder="Dirección de envio" value="<?php if(isset($direccion)) echo $direccion;?>">
                                            <br>
                                             <select class="form-control input-sm" name="provincia" id="provincia" onChange="return provinciaListOnChange()" value="<?php if(isset($provincia)) echo $provincia;?>">
                                                <option >Seleccione su provincia...</option>        
                                                <?php
                                                $xml = simplexml_load_file('provincias/provinciasypoblaciones.xml');
                                                $result = $xml->xpath("/lista/provincia/nombre | /lista/provincia/@id");

                                                for ($i = 0; $i < count($result); $i+=2) {
                                                    $e = $i + 1;
                                                    $resprovincia = $result[$e];
                                                    if($result[$i]==$usuario["provincia"]){
                                                    echo("<option value='$result[$i]' selected>$resprovincia</option>");

                                                         }else{
                                                    echo("<option value='$result[$i]'>$resprovincia</option>");
                                                        }
                                                    }
                                                ?>
                                            </select>
                                            <br>
                                            <label for="localidad">Localidad</label>
                                        <select class="form-control input-sm" name="localidad" id="municipio" >
                                            <option selected ><?php echo $localidad ?></option>
                                        </select> 
                                            <span id="advice"> </span>
                                            <br>
                                             <input type="text" minlength="5" minlength="5" class="form-control" id="cp" name="cp"  placeholder="Código Postal: 00000" value="<?php if(isset($cp)) echo $cp?>">
                                             <br>
                                             <input type="button"  value="Volver" onclick="window.location.href = './index.php';" name="cancelar" >
                                              <input type="submit" value="Guardar Datos" name="guardar">
                                        </div>                                                
                                </div>                                              
                            </form>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        <!--Pie de pagina footer-->
        <!--Fin del contenedor-->
        <?php
       include './pie.php';
       ?>
        <script src="./js/AjaxCode.js"></script>
        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery.complexify.js"></script>
        <script src="js/validarRegister.js"></script>

    </body>
</html>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_end_flush();
