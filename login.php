<?php
session_start();
ob_start();
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>UPOshop</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.css">
    </head>
    <body>
        <?php
        include './modelos/usuarios.php';
        include './cabecera.php';
        if (isset($_POST['login'])) {

            $filtros = Array(
                'email' => FILTER_SANITIZE_MAGIC_QUOTES,
                'password' => FILTER_SANITIZE_MAGIC_QUOTES
            );
            $result = filter_input_array(INPUT_POST, $filtros);
            if (!is_numeric($result['email']) && $result['email'] != "") {
                $email = $result['email'];
            } else {
                $errores[] = "No ha introducido el email";
            }
            if (!is_numeric($result['password']) && $result['password'] != "") {
                $password = $result['password'];
            } else {
                $errores[] = "El password debe de ser completo";
            }
            if (!isset($errores)) {
                $existe = existeUsuario($email, $password);
                if ($existe == True) {
                    $_SESSION["logueado"] = True;
                    $_SESSION["email"] = $email;
                    header("Location:index.php");
                } else {
                    $errores[] = "EL USUARIO NO EXISTE";
                }
            }
        }
        ?>
        <div class="pagina-login">
            <div class="container">
                <div class="row">
                    
                    
                        <?php
                        if (isset($errores)) {
                            ?>
                            <div class="alert alert-danger" role="alert">
                                <ul>
                                    <?php
                                    foreach ($errores as $error) {
                                        ?>
                                        <li><?php echo $error; ?></li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                        <?php } ?>
                    <form class="login" method="POST" action="#">
                        
                        <h2 class="login-heading">Login</h2>
                        <label for="email" class="sr-only">Usuario</label>
                        <input type="email" id="email" name="email" class="form-control" placeholder="Email" value="<?php if (isset($email)) echo $email; ?>" onchange="checkEmail(this)">
                        <label id="emailError" name="emailError"></label>

                        <label for="inputPassword" class="sr-only">Password</label>
                        <input type="password" id="password" name="password" class="form-control" placeholder="Password">
                        <div class="checkbox">
                            <label>
                                <p>Sino se encuentra Registrado <a href='./register.php'>Registrarse</a>.</p>
                            </label>
                            <label>
                                <input type="checkbox" value="remember-me"> Recuerdame
                            </label>
                        </div>
                        <button class="btn btn-lg btn-primary btn-block" type="submit" name="login">Entrar</button>
                    </form>

                </div>
            </div>
        </div>
        <?php
        include './pie.php';
        ?>

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/validarRegister.js"></script>
    </body>
</html>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_end_flush();
