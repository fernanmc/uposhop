
<?php
$cantidadTotal = 0;
$preciototal = 0.00;
if (isset($_POST["anadir"])) {
    $prodcarrito = $_POST["anadir"];
    $preciototal = $preciototal + ($_POST["precio$prodcarrito"] * $_POST["cantidad"]);
    $precio = $_POST["precio$prodcarrito"];
    $cantidadTotal+=$_POST["cantidad"];
    if (!isset($_COOKIE["carrito"][$prodcarrito])) {

        setcookie("carrito[$prodcarrito][id]", $prodcarrito, time() + (60 * 60 * 24));
        setcookie("carrito[$prodcarrito][cantidad]", $_POST["cantidad"], time() + (60 * 60 * 24));
        setcookie("carrito[$prodcarrito][precio]", $precio, time() + (60 * 60 * 24));
    } else {
        setcookie("carrito[$prodcarrito][cantidad]", $_COOKIE["carrito"][$prodcarrito]["cantidad"] + $_POST["cantidad"], time() + (60 * 60 * 24));
    }
    unset($_POST['precio']);
    unset($_POST['anadir']);
    unset($_POST['cantidad']);
}

if (isset($_COOKIE["carrito"])) {
    foreach ($_COOKIE["carrito"] as $encesta) {
        $cantidadTotal+=$encesta["cantidad"];
        $preciototal = $preciototal + ($encesta["precio"] * $encesta["cantidad"]);
    }
}

include './modelos/tipos.php';
?>
<header class="cabecera">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="menuUsuario">
                    <ul>
                        <li><a href="mycount.php"><i class="fa fa-cog"></i> Mi Cuenta</a></li>
                        <li><a href="cart.php"><i class="fa fa-shopping-cart"></i> Mi Carro</a></li>

                        <li><a href="checkouts.php"><i class="fa fa-user"></i>Mis Compras</a></li>
                        <?php
                        if (isset($_SESSION["logueado"])) {
                            if ($_SESSION["logueado"] == True) {
                                ?>
                                <li><a href = "logout.php"><i class = "fa fa-sign-out"></i> Salir</a></li>
                                <?php
                            }
                        } else {
                            ?>
                            <li><a href = "login.php"><i class = "fa fa-sign-in"></i> Login</a></li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header> 
<div class="fondocorp">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="logo">
                    <h1><a href="./index.php">U<span>POShop</span></a></h1>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="carrito">
                    <a href="cart.php">Carrito - <span class="cart-amunt" id="precioCarro"><?php echo number_format($preciototal,2); ?></span> <i class="fa fa-shopping-cart"></i> <span class="productocarro"><?php echo $cantidadTotal; ?></span></a>
                </div>
            </div>
        </div>
    </div>
</div> <!-- End site branding area -->
<!--Menu principal head-->
<nav id="menucompras" class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" >
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">UPOShop</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <!--<li class="active"><a href="#">Ordenadores <span class="sr-only">(current)</span></a></li>-->
                <?php
                $padres = consultarTiposPadres();
                if (count($padres) > 1) {
                    foreach ($padres as $padre) {
                        if (isset($padre['idTipo'])) {
                            echo "<li class = 'dropdown'>";
                            echo "<a href = '#' class = 'dropdown-toggle' data-toggle = 'dropdown' role = 'button' aria-haspopup = 'true' >" . $padre['nombre'] . "<span class = 'caret'></span></a>";
                            $hijos = consultarHijos($padre['idTipo']);
                            if (count($hijos) > 1) {
                                echo "<ul class = 'dropdown-menu'>";
                                foreach ($hijos as $hijo) {
                                    echo "<li><a href = 'index.php?action=categoria&id=" . $hijo['idTipo'] . "'>" . $hijo['nombre'] . "</a></li>";
                                }
                                echo "</ul>";
                            }
                            echo "</li>";
                        }
                    }
                }
                ?>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
