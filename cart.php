<!DOCTYPE html>
<html lang="es">
    <?php
    ob_start();
    session_start();
    ?>
    <head>
        <meta charset="UTF-8">
        <title>UPOShop</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.css">
    </head>
    <body>
        <?php
        include './modelos/productos.php';

        if (isset($_POST["checkout"])) {
            foreach ($_COOKIE["carrito"] as $productoCarro) {
                $productoStock = consultarProducto($productoCarro["id"]);
                if ($productoCarro["cantidad"] <= $productoStock["stock"]) {
                    setcookie("carrito[$id][cantidad]", $cantidad, time() + (60 * 60 * 24));
                    $_COOKIE["carrito"][$id]["cantidad"] = $cantidad;
                } else {

                    $errores[] = "El producto " . $productoStock["nombre"] . " Solo tiene " . $productoStock["stock"] . " unidades disponibles";
                }
                if (!isset($errores)) {
                    header("Location: precheckout.php");
                }
            }
        }
        if (isset($_POST["borrar"])) {
            foreach ($_COOKIE["carrito"] as $productoCarro) {
                $id = $productoCarro["id"];
                $borrar = $_POST["borrar"];
                if (isset($borrar)) {
                    setcookie("carrito[$borrar][id]", "", time() - (60 * 60 * 24));
                    setcookie("carrito[$borrar][cantidad]", "", time() - (60 * 60 * 24));
                    setcookie("carrito[$borrar][precio]", "", time() - (60 * 60 * 24));
                }
                header("Location: cart.php");
            }
        }
        if (isset($_POST["actualizar"])) {
            if (isset($_COOKIE["carrito"])) {
                foreach ($_COOKIE["carrito"] as $productoCarro) {
                    $cantidad = $_POST[$productoCarro["id"]];
                    $id = $productoCarro["id"];

                    setcookie("carrito[$id][cantidad]", $cantidad, time() + (60 * 60 * 24));
                    $_COOKIE["carrito"][$id]["cantidad"] = $cantidad;


                    //  $errores[] = "El producto " . $productoStock["nombre"] . " Solo tiene " . $productoStock["stock"] . " unidades disponibles";
                }
            }
        }

        include './cabecera.php';


        // setcookie ("totalCantidad",$cantidadTotal, time() + (60*60*24)); 
        // setcookie ("totalCarrito",$preciototal, time() + (60*60*24));
        if (isset($_POST["search"])) {
            $busqueda = $_POST["busqueda"];
            header("Location: index.php?action=search&bus=$busqueda");
        }
        ?>
        <div class="pagina-producto">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="producto-sidebar">
                            <h2 class="sidebar-titulo">Buscar Productos</h2>
                            <form method="POST" action="#">
                                <input type="text" placeholder="Buscar productos..." name="busqueda" id="busqueda">
                                <input type="submit" value="Buscar" name="search">
                            </form>
                        </div>

                        <div class="producto-sidebar">
                            <h2 class="sidebar-titulo">Productos</h2>
                            <?php
                            $productosRelacionados = consultarProductosRecientes();
                            foreach ($productosRelacionados as $relacionado) {
                                if (isset($relacionado["idProducto"])) {
                                    $fotorelacionado = listarFoto($relacionado['idProducto']);
                                    ?>
                                    <div class="miniatura-reciente">
                                        <img src="img/<?php echo $fotorelacionado[0]; ?>" class="miniatura" alt="">
                                        <h2><a href="product.php?idProduct=<?php echo $relacionado['idProducto'] ?>"><?php echo $relacionado["nombre"]; ?></a></h2>
                                        <div class="producto-sidebar-precio">
                                            <ins><?php echo $relacionado["precio"] . " €"; ?></ins>
                                        </div>                             
                                    </div>
                                    <?php
                                }
                            }
                            ?>

                        </div>

                        <div class="producto-sidebar">
                            <h2 class="sidebar-titulo">Publicaciones recientes</h2>
                            <ul>
                                <?php
                                $productosRecientes = consultarProductosRecientes();
                                foreach ($productosRecientes as $reciente) {
                                    if (isset($reciente["idProducto"])) {
                                        ?>
                                        <li><a href="product.php?idProduct=<?php echo $reciente['idProducto'] ?>"><?php echo $reciente["nombre"] . "-2016" ?></a></li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-8">
                        <div class="product-content-right">
                            <?php
                            if (isset($errores)) {
                                ?>
                                <div class="alert alert-danger" role="alert">
                                    <ul>
                                        <?php
                                        foreach ($errores as $error) {
                                            ?>
                                            <li><?php echo $error; ?></li>
                                            <?php
                                        }
                                        unset($errores);
                                        ?>
                                    </ul>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="woocommerce">
                                <form method="post" action="#">
                                    <table cellspacing="0" class="tabla_carro cart">
                                        <thead>
                                            <tr>
                                                <th class="borrar-producto">&nbsp;</th>
                                                <th class="producto-miniatura">&nbsp;</th>
                                                <th class="producto-nombre">Producto</th>
                                                <th class="producto-precio">Precio ud</th>
                                                <th class="producto-cantidad">Cantidad</th>
                                                <th class="product-subtotal">Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            // var_dump($_COOKIE["carrito"]);
                                            if (isset($_COOKIE["carrito"])) {
                                                foreach ($_COOKIE["carrito"] as $productoCarro) {

                                                    //  var_dump($productoCarro)   
                                                    $producto = consultarProducto($productoCarro["id"]);
                                                    $imagen = listarFoto($productoCarro["id"]);
                                                    $productoCarro["cantidad"] = $_COOKIE["carrito"][$productoCarro["id"]]["cantidad"];
                                                    //echo $imagen[0];
                                                    ?>
                                                    <tr>
                                                        <td class="borrar-producto">
                                                            <button type="submit"  class="btn btn-info" name="borrar" id="borrar"  value="<?php echo $producto['idProducto'] ?>" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-trash"></i>
                                                            </button> 
                                                        </td>

                                                        <td class="producto-miniatura">
                                                            <a href="product.php?idProduct=<?php echo $producto["idProducto"]; ?>"><img width="145" height="145" alt="poster_1_up" class="shop_thumbnail" src="img/<?php echo $imagen[0] ?>"></a>
                                                        </td>

                                                        <td class="producto-nombre">
                                                            <a href="product.php?idProduct=<?php echo $producto["idProducto"]; ?>"><?php echo $producto["nombre"]; ?></a> 
                                                        </td>

                                                        <td class="producto-precio">
                                                            <span class="precio"><?php echo $producto["precio"] . " €"; ?></span> 
                                                        </td>

                                                        <td class="producto-cantidad">
                                                            <div class="cantidad">
                                                                <input type="number"  class="cantidad" id="<?php echo $productoCarro["id"]; ?>" name="<?php echo $productoCarro["id"]; ?>" title="Qty" value="<?php echo $productoCarro["cantidad"]; ?>" min="0" step="1">
                                                            </div>
                                                        </td>

                                                        <td class="product-subtotal">
                                                            <span class="precio"><?php echo $productoCarro["cantidad"] * $productoCarro["precio"] . " €"; ?></span> 
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                            <tr>
                                                <td class="acciones" colspan="6">

                                                    <input type="submit" value="Actualizar Carrito" name="actualizar" >
                                                    <input type="submit" value="Realizar Pedido" name="checkout">
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                    <div class="total_carro ">
                                        <h2>Compra total</h2>

                                        <table cellspacing="0">
                                            <tbody>
                                                <!--<tr class="carro-subtotal">
                                                    <th>Cart Subtotal</th>
                                                    <td><span class="precio">£15.00</span></td>
                                                </tr>!-->

                                                <tr>
                                                    <th>Env&iacute;o y manipulaci&oacute;n</th>
                                                    <td>Env&iacute;o GRATIS!</td>
                                                </tr>

                                                <tr>
                                                    <th>Compra Total</th>
                                                    <td><strong><span class="precio"><?php echo $preciototal . " €"; ?></span></strong> </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Pie de pagina footer-->
        <!--Fin del contenedor-->
        <?php
        include './pie.php';
        ?>
        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.js"></script>

    </body>
</html>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_end_flush();
