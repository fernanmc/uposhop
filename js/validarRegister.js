function fortaleza() {
                $(function () {
                    $("#inputPassword").complexify({}, function (valid, complexity) {
                        if (valid)
                            document.getElementById("progressbar").value = complexity;
                    });
                });
            }
function checkEmail(input) {
    var expr = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
    var email = document.getElementById(input.id);
    var label = document.getElementById("emailError");

    if (!expr.test(email.value)) {
        $(input).css("borderColor", "red");
        $(label).html("<p>No cumple las normas de email</p>");
    }else{
        $(input).css("borderColor", "");
        $(label).html("");
    }
}

/*
 *  - matches a string of six or more characters;
 *  - that contains at least one digit (\d is shorthand for [0-9]);
 *  - at least one lowercase character; and
 *  - at least one uppercase character:
 * 
 */
function checkPassword(input) {
    var expr = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
    var password = document.getElementById(input.id);
    var label = document.getElementById("passwordError");

    if (!expr.test(password.value)) {
        $(input).css("borderColor", "red");
        $(label).html("<p>No cumple las normas de password</p>");
    }else{
        $(input).css("borderColor", "");
        $(label).html("");
    }
}

function checkConfirmPassword(input) {
    var password = document.getElementById('inputPassword');
    var repeatPassword = document.getElementById('repeatPassword');
    var label = document.getElementById("repeatPasswordError");

    if (password.value !== repeatPassword.value) {
        $(input).css("borderColor", "red");
        $(label).html("<p>Las password no coinciden.</p>");
    }else{
        $(input).css("borderColor", "");
        $(label).html("");
    }
}