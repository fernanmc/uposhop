<!DOCTYPE html>
<html lang="es">
<?php
session_start();

?>
    <head>
        <meta charset="UTF-8">
        <title>UPOShop</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.css">
    </head>
    <body>
        <?php
        include './cabecera.php';
        include './modelos/productos.php';
        ?>
        <?php
        if (isset($_GET['action'])) {
            $productos = "";
            switch ($_GET['action']) {
                case 'categoria':
                    $productos = consultarProductosCategoria($_GET['id']);
                    break;
                 case 'search':
                    $cadena=$_GET["bus"];
                    $productos = consultarBusqueda($cadena);
                    break;
                default :
                    $productos = consultarProductos();
                    break;
            }
        } else {
            $productos = consultarProductos();
        }
        
        ?>
        <!--Inicio del contenedor-->
        <div id="contenidoCentral" class="container"> 
            <div class="row">
                <form method="post" action="#">
                <br><br>
                <?php
                if (count($productos) > 1) {
                    foreach ($productos as $producto) {
                        if (isset($producto['idProducto'])) {
                            ?>
                            <div class="col-lg-4">
                                <div class="producto">
                                    <?php
                                    $fotos = listarFotos($producto['idProducto']);
                                    $cont = 0;
                                    if (count($fotos) > 1) {
                                        $fotoprincipal = listarFoto($producto['idProducto']);
                                        ?>

                                        <a href="product.php?idProduct=<?php echo $producto['idProducto'] ?>"><img src="img/<?php echo $fotoprincipal[0]; ?>" alt="Generic placeholder image"></a>
                                        <h2><?php echo $producto['nombre'] ?></h2>
                                        <p><span class="badge"><?php echo $producto['precio'] ?> €</span></p>
                                        <p></p>
                                        <p>
                                            <button type="button" class="btn btn-default" title="<?php echo $producto['nombre'] ?>" data-container="body" data-toggle="popover" data-placement="top" data-content="<?php echo $producto['caracteristicas'] ?>">
                                                Ver detalles <i class="fa fa-eye"></i>
                                            </button>  
                                            <button type="submit"  class="btn btn-info" name="anadir" id="anadir" value="<?php echo $producto['idProducto']?>" data-toggle="tooltip" data-placement="bottom" title="Añadir este producto al carrito">Al carrito <i class="fa fa-shopping-cart"></i>
                                            </button>
                                            <input type="hidden" name="<?php echo "precio".$producto["idProducto"]; ?>" value="<?php echo $producto['precio']; ?>"/>
                                            <input type="hidden" name="cantidad" value="1"/>
                                        </p>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                    }
                }
                ?>
                </form>
            </div>
        </div>

        <!--Pie de pagina footer-->
        <!--Fin del contenedor-->


        <?php
        include './pie.php';
        ?>
        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.js"></script>
        <script>
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
            $(function () {
                $('[data-toggle="popover"]').popover()
            });
        </script>


    </body>
</html>
