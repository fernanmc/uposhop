<!DOCTYPE html>
<html lang="es">
<?php
session_start();
ob_start();
if(isset($_SESSION["logueado"])){
   $idUser=$_SESSION["email"];
}else{
     session_destroy();
     header("Location: login.php");
}

?>
    <head>
        <meta charset="UTF-8">
        <title>UPOShop</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.css">
    </head>
    <body>
       <?php
        
       include './cabecera.php';
       include './modelos/productos.php';
       include './modelos/compras.php';
        
      

       ?>
        <div class="pagina-producto">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="producto-sidebar">
                        <h2 class="sidebar-titulo">Buscar Productos</h2>
                        <form method="POST" action="#">
                            <input type="text" placeholder="Buscar Productos..." name="busqueda" id="busqueda">
                            <input type="submit" value="Search" name="search">
                        </form>
                    </div>

                         <div class="producto-sidebar">
                        <h2 class="sidebar-titulo"> PRODUCTOS</h2>
                        <?php
                          $productosRelacionados= consultarProductosRecientes();
                          foreach ($productosRelacionados as $relacionado) {
                               if(isset($relacionado["idProducto"])){
                               $fotorelacionado=  listarFoto($relacionado['idProducto']);
                              
                        ?>
                        <div class="miniatura-reciente">
                            <img src="img/<?php echo $fotorelacionado[0];?>" class="miniatura" alt="">
                            <h2><a href="product.php?idProduct=<?php echo $relacionado['idProducto'] ?>"><?php echo $relacionado["nombre"];?></a></h2>
                            <div class="producto-sidebar-precio">
                                <ins><?php echo $relacionado["precio"]." €";?></ins>
                            </div>                             
                        </div>
                        <?php
                               }
                        }
                        ?>
                       
                    </div>

                        <div class="producto-sidebar">
                        <h2 class="sidebar-titulo">ÚLTIMOS PRODUCTOS</h2>
                        <ul>
                              <?php
                          $productosRecientes= consultarProductosRecientes();
                          foreach ($productosRecientes as $reciente) {
                              if(isset($reciente["idProducto"])){
                        ?>
                            <li><a href="product.php?idProduct=<?php echo $reciente['idProducto'] ?>"><?php echo  $reciente["nombre"]."-2016" ?></a></li>
                            
                              <?php
                              
                              }
                        }
                        ?>
                        </ul>
                    </div>
                </div>
                  
                    <div class="col-md-8">
                    <div class="product-content-right">
                      
                        
                            
                                <table cellspacing="0" class="tabla_carro cart">
                                    <thead>
                                        <tr>
                                            <th class="producto-miniatura">Fecha Compra</th>
                                            <th class="producto-nombre">Productos</th>
                                            <th class="producto-precio">Estado</th>
                                            <th class="producto-cantidad">Fecha Actualización</th>
                                            <th class="product-subtotal">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        
                                        $compras=consultarCompras($idUser);
                                        if(count($compras)>0){
                                        foreach ($compras as $compra) {
                                           if(isset($compra["idCompra"])){
                                               
                                          
                                           $products="";
                                           $productosCompra= consultarProductosCompras($compra["idCompra"]);
                                           foreach ($productosCompra as $productoCompra) {
                                               if(isset($productoCompra["idCompra"])){
                                                   $products.=$productoCompra["nombre"]." Cant-".$productoCompra["cantidad"]."<br>";
                                               }
                                                 
                                            }
                                          
                                        ?>
                                        <tr>
                                            

                                            <td class="producto-miniatura">
                                                <?php echo $compra["fechaRegistro"] ?>
                                            </td>

                                            <td class="producto-nombre">
                                                <a href="buy.php?idCompra=<?php echo $compra["idCompra"]; ?>"><?php echo $products; ?></a> 
                                            </td>

                                           

                                            <td class="producto-cantidad">
                                                <div class="cantidad">
                                                    <input type="text" readonly  class="cantidad"  title="Qty" value="<?php echo $compra["estado"]; ?>" min="0" step="1">
                                                    
                                                </div>
                                            </td>
                                             <td class="producto-precio">
                                                <span class="precio"><?php echo $compra["fechaActualizacion"].""; ?></span> 
                                            </td>
                                            <td class="product-subtotal">
                                               <span class="precio"><?php echo $compra["precioTotal"]." €"; ?></span>  
                                            </td>
                                        </tr>
                                         <?php
                                           }
                                          }  
                                        } 
                                        ?>
                                        <tr>
                                            <td class="acciones" colspan="6">
                                                
                                                <input type="button"  value="Volver" onclick="window.location.href = './index.php';" name="Volver" >
                                             
                                            </td>
                                        </tr>
                                       
                                    </tbody>
                                </table>
                            </div>
                                                     
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
        <!--Pie de pagina footer-->
        <!--Fin del contenedor-->
        <?php
       include './pie.php';
       ?>
        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.js"></script>

    </body>
</html>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_end_flush();