<!DOCTYPE html>
<html lang="es">

    <?php
    session_start();
    ob_start();
    ?>
    <head>
        <meta charset="UTF-8">
        <title>UPOShop</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap-lightbox.css">
    </head>

    <body>
        <?php
        if (isset($_POST["search"])) {
            $busqueda = $_POST["busqueda"];
            header("Location: index.php?action=search&bus=$busqueda");
        }
        ?>
        <?php
        include './cabecera.php';
        include './modelos/productos.php';
        include './modelos/opiniones.php';
        // echo $_GET['idProduct'];

        if (isset($_POST['envioReview'])) {
            $filtros = Array(
                'rating' => FILTER_SANITIZE_NUMBER_INT,
                'review' => FILTER_SANITIZE_MAGIC_QUOTES
            );
            $result = filter_input_array(INPUT_POST, $filtros);

            if ($result['review'] == "") {
                $errores[] = "Debe escribir un comentario.";
            } else {
                $idUser = $_SESSION["email"];
                $idProducto = $_POST['idProducto'];
                $valoracion = $_POST['rating'];
                $comentario = $_POST['review'];

                if (!isset($errores)) {
                    $insertado = insertarOpinion($idUser, $idProducto, $valoracion, $comentario);
                    if ($insertado) {
                        header('Location: product.php?idProduct=' + $idProducto);
                    } else {
                        $errores[] = "Ha habido un error";
                    }
                }
            }
        }

        $url = basename($_SERVER ["PHP_SELF"]);
        if (isset($_GET['idProduct'])) {

            $producto = consultarProducto($_GET['idProduct']);
            $idproducto = $_GET['idProduct'];
            $url = $url . "?idProduct=$idproducto";
        }
        ?>
        <div class="pagina-producto">
            <div class="container">
                <?php
                if (isset($errores)) {
                    ?>
                    <div class="alert alert-danger" role="alert">
                        <ul>
                            <?php
                            foreach ($errores as $error) {
                                ?>
                                <li><?php echo $error; ?></li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                    <?php
                }
                ?>
                <div class="row">
                    <div class="col-md-4">
                        <div class="producto-sidebar">
                            <h2 class="sidebar-titulo">Buscar Productos</h2>
                            <form method="POST" action="#">
                                <input type="text" placeholder="Buscar Productos..." name="busqueda" id="busqueda">
                                <input type="submit" value="Search" name="search">
                            </form>
                        </div>

                        <div class="producto-sidebar">
                            <h2 class="sidebar-titulo"> PRODUCTOS</h2>
                            <?php
                            $productosRelacionados = consultarProductosRelacionados($producto["idTipo"]);
                            foreach ($productosRelacionados as $relacionado) {
                                if (isset($relacionado["idProducto"])) {
                                    $fotorelacionado = listarFoto($relacionado['idProducto']);
                                    ?>
                                    <div class="miniatura-reciente">
                                        <img src="img/<?php echo $fotorelacionado[0]; ?>" class="miniatura" alt="">
                                        <h2><a href="product.php?idProduct=<?php echo $relacionado['idProducto'] ?>"><?php echo $relacionado["nombre"]; ?></a></h2>
                                        <div class="producto-sidebar-precio">
                                            <ins><?php echo $relacionado["precio"] . " €"; ?></ins>
                                        </div>                        
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                            
                        </div>

                        <div class="producto-sidebar">
                            <h2 class="sidebar-titulo">ÚLTIMOS PRODUCTOS</h2>
                            <ul>
                                <?php
                                $productosRecientes = consultarProductosRecientes();
                                foreach ($productosRecientes as $reciente) {
                                    if (isset($reciente["idProducto"])) {
                                        ?>
                                        <li><a href="product.php?idProduct=<?php echo $reciente['idProducto'] ?>"><?php echo $reciente["nombre"] . "-2016" ?></a></li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-8">
                        <div class="producto-contenido">
                            <?php
                            if (isset($producto)) {
                                $tipo = consultarTipo($producto['idTipo']);
                                $opiniones = consultarOpiniones($producto['idProducto']);
                                ?>
                                <div class="producto-categorias">
                                    <a href="index.php">Home</a>
                                    <a href="index.php?id=<?php echo $tipo['idTipo'] ?>"><?php echo $tipo['nombre'] ?></a>
                                    <a href="product.php?idProduct=<?php echo $producto['idProducto'] ?>"><?php echo $producto['nombre'] ?></a>
                                </div>
                                <div id="lightbox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <button type="button" class="close hidden" data-dismiss="modal" aria-hidden="true">x</button>
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <img src="" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="producto-imagenes">
                                            <?php
                                            $fotos = listarFotos($producto['idProducto']);
                                            $cont = 0;
                                            if (count($fotos) > 1) {
                                                $fotoprincipal = listarFoto($producto['idProducto']);
                                                ?>
                                                <div class="producto-imagen-principal">
                                                    <div class="col-xs-8">
                                                        <a href="#" class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                                            <img src="img/<?php echo $fotoprincipal[0]; ?>" alt="Imagen..." onclick="mostrar()">
                                                        </a>
                                                    </div>
                                                </div>

                                                <div class="producto-galeria">
                                                    <?php
                                                    foreach ($fotos as $foto) {
                                                        $cont++;
                                                        if (isset($foto['enlace'])) {
                                                            $link = $foto['enlace'];
                                                            ?>
                                                            <div class="col-xs-6">
                                                                <a href="#" class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                                                    <img src="img/<?php echo $link ?>" alt="Imagen..." onclick="mostrar()">
                                                                </a>
                                                            </div>

                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>

                                            </div>

                                            <div class="comentarios">
                                                <?php
                                                if (isset($opiniones)) {
                                                    foreach ($opiniones as $opinion) {
                                                        if (isset($opinion['idOpinion'])) {
                                                            ?>
                                                            <div class="panel panel-default">
                                                                <div class="panel-comentarios">
                                                                    <strong><?php echo $opinion['idUsuario'] ?></strong> 
                                                                    <strong class="rating"><?php echo "Valoración: " . $opinion['valoracion']; ?></strong><br />
                                                                    <span class="text-muted"><?php echo $opinion['fecha'] ?></span>
                                                                </div>
                                                                <div class="panel-body">
                                                                    <?php echo $opinion['comentario'] ?>
                                                                </div><!-- /panel-body -->
                                                            </div><!-- /panel panel-default -->
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="col-sm-6">
                                        <div class="producto-descripcion">
                                            <h2 class="producto-nombre"><?php echo $producto['nombre'] ?></h2>
                                            <div class="producto-descripcion-precio">
                                                <ins><?php echo $producto['precio'] . " €" ?></ins> <!--<del><?php // echo $producto['precio']                                     ?></del>!-->
                                            </div>
                                            <div class="producto-descripcion-precio">
                                                <ins><?php
                                                    if ($producto['stock'] > 0) {
                                                        echo "DISPONOBLE: " . $producto['stock'] . " uds.";
                                                    } else {
                                                        echo "No disponible.";
                                                    }
                                                    ?></ins>
                                            </div>

                                            <form method="post" action="#"  class="carritoform">
                                                <div class="cantidad">
                                                    <input type="number"  value="1" name="cantidad" id="cantidad" min="1" step="1">
                                                </div>
                                                <button class="anadirCarro" type="submit" name="anadir" id="anadir" value="<?php echo $producto["idProducto"]; ?>">Añadir al carro</button>
                                                <input type="hidden" name="<?php echo "precio" . $producto["idProducto"]; ?>" value="<?php echo $producto['precio'] ?>"/>
                                            </form>   

                                            <div class="producto-descripcion-category">
                                                <p>Categoría: <a href="index.php?id=<?php echo $tipo['nombre'] ?>"><?php echo $tipo['nombre'] ?></a></p>
                                            </div> 

                                            <div role="tabpanel">
                                                <ul class="producto-tabla" role="tablist">
                                                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Descripción</a></li>
                                                    <li role="presentation" ><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Opiniones</a></li>
                                                </ul>
                                                <div class="contenido-tabla">
                                                    <div role="tabpanel" class="tab-pane fade in active" id="home">
                                                        <h2>Descripción</h2>  
                                                        <p><?php echo $producto["caracteristicas"]; ?></p>
                                                        <div class="producto-descripcion-precio">
                                                            <ins>
                                                                <?php
                                                                $opiniones = consultarOpiniones($producto['idProducto']);
                                                                if (isset($opiniones)) {
                                                                    $val = 0;
                                                                    foreach ($opiniones as $opinion) {
                                                                        if (isset($opinion['idOpinion'])) {
                                                                            $val += $opinion['valoracion'];
                                                                        }
                                                                    }
                                                                    if ((count($opiniones)-1) > 0) {
                                                                        $val = ($val / (count($opiniones) - 1));
                                                                        echo "Valoración media: " . number_format($val,2);
                                                                    }else{
                                                                        echo "Valoración media: -";
                                                                    }
                                                                }
                                                                ?>
                                                            </ins>
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel"  class="tab-pane fade" id="profile">
                                                        <h2>Opinión</h2>
                                                        <?php
                                                        if (isset($_SESSION["logueado"]) && ($_SESSION["logueado"] == True)) {
                                                            ?>
                                                            <div class="submit-review">
                                                                <form action="#" method="POST">
                                                                    <div class="valoracion">
                                                                        <label>Tu valoración</label>
                                                                        <select name="rating" id="rating">
                                                                            <option value="1">1</option>
                                                                            <option value="2">2</option>
                                                                            <option value="3">3</option>
                                                                            <option value="4">4</option>
                                                                            <option value="5">5</option>
                                                                            <option value="6">6</option>
                                                                            <option value="7">7</option>
                                                                            <option value="8">8</option>
                                                                            <option value="9">9</option>
                                                                            <option value="10" selected>10</option>
                                                                        </select>
                                                                    </div>
                                                                    <label for="review">Tu opinión</label> 
                                                                    <textarea name="review" cols="30" rows="10"></textarea>
                                                                    <input type="hidden" name="idProducto" id="idProducto" value="<?php echo $producto['idProducto'] ?> ">
                                                                    <input type="submit" name="envioReview" value="Submit">
                                                                </form>
                                                            </div>
                                                            <?php
                                                        } else {
                                                            echo "<p>No puede dejar comentarios sin haber <a href='./login.php'>iniciado sesión</a>.</p>";
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>                    
                    </div>
                </div>
            </div>
        </div>
        <?php
        include './pie.php';
        ?>

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/bootstrap-lightbox.js"></script>
        <script type="text/javascript">
                                                        function mostrar() {
                                                            $(document).ready(function () {
                                                                var $lightbox = $('#lightbox');

                                                                $('[data-target="#lightbox"]').on('click', function (event) {
                                                                    var $img = $(this).find('img'),
                                                                            src = $img.attr('src'),
                                                                            alt = $img.attr('alt'),
                                                                            css = {
                                                                                'maxWidth': $('.producto-contenido').width(),
                                                                                'maxHeight': $('.producto-contenido').height()
                                                                            };

                                                                    $lightbox.find('.close').addClass('hidden');
                                                                    $lightbox.find('img').attr('src', src);
                                                                    $lightbox.find('img').attr('alt', alt);
                                                                    $lightbox.find('img').css(css);
                                                                });

                                                                $lightbox.on('shown.bs.modal', function (e) {
                                                                    var $img = $lightbox.find('img');

                                                                    $lightbox.find('.modal-dialog').css({'width': $img.width()});
                                                                    $lightbox.find('.close').removeClass('hidden');
                                                                });
                                                            });
                                                        }
        </script>
    </body>
</html>

