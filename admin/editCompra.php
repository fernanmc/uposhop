<?php
session_start();
if ($_SESSION["logueado"] == True) {
    $idUser = $_SESSION["email"];
} else {
    session_abort();
    header("Location:loginAdmin.php");
}
?>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_start();
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>UPOShop</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">
        <link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.css">
    </head>
    <body>
        <?php
        include './cabeceraAdmin.php';
        include '../modelos/compras.php';

        if (isset($_GET['id'])) {
            $idCompra = $_GET['id'];
            $compra = consultarCompra($idCompra);
            $idUser = $compra['email'];
            $nombre = $compra['nombre'];
            $apellidos = $compra['apellidos'];
            $direccion = $compra['direccion'];
            $telefono = $compra['telefono'];
            $precioTotal = $compra['precioTotal'];
            $estado = $compra['estado'];
            $ciudad = $compra['ciudad'];
            $localidad = $compra['localidad'];
            $cp = $compra['cp'];
            $idPago = $compra['idPago'];
            $fechaRegistro = $compra['fechaRegistro'];
            $fechaActualizacion = $compra['fechaActualizacion'];
        }

        if (isset($_POST['guardar'])) {
            $filtros = Array(
                'precioTotal' => FILTER_SANITIZE_NUMBER_FLOAT,
                'estado' => FILTER_SANITIZE_MAGIC_QUOTES
            );
            $result = filter_input_array(INPUT_POST, $filtros);

            if ($result['precioTotal'] > 0 && $result['precioTotal'] != "") {
                $precioTotal = $result['precioTotal'];
            } else {
                $errores[] = "El precio debe ser positivo y es obligatorio";
            }
            if (!is_numeric($result['estado']) && $result['estado'] != "") {
                $estado = $result['estado'];
            } else {
                $errores[] = "El estado es obligatorio";
            }

            if (!isset($errores)) {
                $insertado = editarCompra($idCompra, $precioTotal, $estado);
                if ($insertado) {
                    header("Location:compras.php");
                } else {
                    $errores[] = "Ha habido un error";
                }
            }
        }
        ?>
        <div class="pagina-producto">
            <div class="container">
                <div class="tab-content">
                    <br/>
                    <?php
                    if (isset($errores)) {
                        ?>
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                <?php
                                foreach ($errores as $error) {
                                    ?>
                                    <li><?php echo $error; ?></li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                        <?php
                    }
                    ?>
                    <br/>
                    <h2>Editar Compra</h2>
                    <form role="form" method="POST" action="#">
                        <div class="form-group">
                            <label for="email">Usuario</label>
                            <input type="text" class="form-control" id="email" name="email"  value="<?php echo $idUser ?>" readonly>
                        </div> 
                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control" id="nombre" name="nombre"  placeholder="Introduzca el nombre" value="<?php echo $nombre ?>" readonly>
                        </div>
                        <div class="form-group">
                            <label for="apellidos">Apellidos</label>
                            <input type="text" class="form-control" id="apellidos" name="apellidos"  value="<?php echo $apellidos ?>" readonly>
                        </div>
                        <div class="form-group">
                            <label for="direccion">Direccion</label>
                            <input type="text" class="form-control" id="direccion" name="direccion"  placeholder="Introduzca la direccion" value="<?php echo $direccion ?>" readonly>
                        </div>
                        <div class="form-group">
                            <label for="telefono">Telefono</label>
                            <input type="text" class="form-control" id="telefono" name="telefono"  placeholder="Introduzca el telefono" value="<?php echo $telefono ?>" readonly>
                        </div>
                        <div class="form-group">
                            <label for="precioTotal">Precio Total</label>
                            <input type="text" class="form-control" id="precioTotal" name="precioTotal"  placeholder="Introduzca el precio Total" value="<?php echo $precioTotal ?>" >
                        </div>
                        <div class="form-group">
                            <label for="estado">Estado</label>
                            <select id="estado" name="estado"  placeholder="Introduzca el estado"type="text" class="form-control" id="estado" name="estado"  placeholder="Introduzca el estado" value="<?php echo $estado ?>">
                               <option value="Pendiente envio">Pendiente de Envio</option>
                                <option value="Enviado">Enviado</option>
                                <option value="En reparto">en reparto</option>
                                <option value="Entregado">Entregado</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="ciudad">Ciudad</label>
                            <input type="text" class="form-control" id="ciudad" name="ciudad"  placeholder="Introduzca la ciudad" value="<?php echo $ciudad ?>" readonly>
                        </div>
                        <div class="form-group">
                            <label for="localidad">Localidad</label>
                            <input type="text" class="form-control" id="localidad" name="localidad"  placeholder="Introduzca la localidad" value="<?php echo $localidad ?>" readonly>
                        </div>
                        <div class="form-group">
                            <label for="cp">Codigo Postal</label>
                            <input type="text" class="form-control" id="cp" name="cp"  placeholder="Introduzca el cp" value="<?php echo $cp ?>" readonly>
                        </div>
                        <div class="form-group">
                            <label for="idPago">ID Pago</label>
                            <input type="text" class="form-control" id="idPago" name="idPago"  placeholder="Introduzca el idPago" value="<?php echo $idPago ?>" readonly>
                        </div>
                        <div class="form-group">
                            <label for="fechaRegistro">Fecha Registro</label>
                            <input type="text" class="form-control" id="fechaRegistro" name="fechaRegistro"  placeholder="Introduzca fecha Registro " value="<?php echo $fechaRegistro ?>" readonly>
                        </div>
                        <div class="form-group">
                            <label for="fechaActualizacion">Fecha actualizacion</label>
                            <input type="text" class="form-control" id="fechaActualizacion" name="fechaActualizacion"  placeholder="Introduzca fecha Actualizacion" value="<?php echo $fechaActualizacion ?>" readonly>
                        </div>
                        <button type="submit" name="guardar" class="btn btn-primary">Guardar</button>
                        <button type="button" class="btn btn-success" onClick="window.location.href = 'compras.php';" >Volver</button>
                    </form>
                    <br/>
                </div>
            </div>
        </div>
        <?php
        include '../pie.php';
        ?>
        <script src="../js/jquery-1.11.1.min.js"></script>
        <script src="../js/bootstrap.js"></script>
    </body>
</html>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_end_flush();

