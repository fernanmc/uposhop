<?php
session_start();
if ($_SESSION["logueado"] == True) {
    $idUser = $_SESSION["email"];
} else {
    session_abort();
    header("Location:loginAdmin.php");
}
?>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_start();
?>
<!DOCTYPE html>
<html lang="es">
    <head>

        <meta charset="UTF-8">
        <title>UPOShop</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">
        <link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.css">
       
    </head>
    <body>
        <?php
        include 'cabeceraAdmin.php';
        include '../modelos/usuarios.php';
        
        $password = null;
        if (isset($_GET['id'])) {
            $idAdmin = $_GET['id'];
            $usuario = consultarUser($idAdmin);
            $email = $usuario['email'];
            $password = $usuario['password'];
            $nombre = $usuario['nombre'];
            $apellidos = $usuario['apellidos'];
            $ciudad = $usuario['ciudad'];
            $provincia = $usuario['provincia'];
            $cp = $usuario['cp'];
            $direccion = $usuario['direccion'];
            $telefono = $usuario['telefono'];
        }
        if (isset($_POST['guardar'])) {
            $filtros = Array(
                'email' => FILTER_SANITIZE_EMAIL,
                'password' => FILTER_SANITIZE_MAGIC_QUOTES,
                'nombre' => FILTER_SANITIZE_MAGIC_QUOTES,
                'apellidos' => FILTER_SANITIZE_MAGIC_QUOTES,
                'direccion' => FILTER_SANITIZE_MAGIC_QUOTES,
                'localidad' => FILTER_SANITIZE_MAGIC_QUOTES,
                'provincia' => FILTER_SANITIZE_MAGIC_QUOTES,
                'cp' => FILTER_SANITIZE_MAGIC_QUOTES,
                'telefono' => FILTER_SANITIZE_MAGIC_QUOTES,
            );
            $result = filter_input_array(INPUT_POST, $filtros);
            if (!is_numeric($result['email']) && $result['email'] != "") {
                $resultemail = $result['email'];
                if ($resultemail != $email) {
                    if (comprobarEmail($result['email']) != true) {
                        $errores[] = "el email ya existe en el sistema";
                    } else {
                        $email = $resultemail;
                    }
                }
            } else {
                $errores[] = "el email es obligatorio";
            }
            //echo $result["password"];
            if ($result['password'] != "" && strlen($result['password']) > 7) {
                $password = $result['password'];
                $password = password_hash($password, PASSWORD_BCRYPT, ['cost' => 12]);
               // echo $password;
            }
            if ($result['nombre'] != "") {
                $nombre = $result['nombre'];
            } else {
                $nombre = null;
            }
            if ($result['apellidos'] != "") {
                $apellidos = $result['apellidos'];
            } else {
                $apellidos = null;
            }
            if ($result['direccion'] != null) {
                $direccion = $result['direccion'];
            } else {
                $direccion = null;
            }
            if ($result['direccion'] != "") {
                $direccion = $result['direccion'];
            } else {
                $direccion = "";
            }
            if ($result['localidad'] != "Seleccione antes una localidad") {
                $ciudad = $result['localidad'];
            } else {
                $ciudad = "";
            }
            if ($result['cp'] != "") {
                if (cp_valido($result['cp'])) {
                    $cp = $result['cp'];
                } else {
                    $errores[] = "El código postal no es correcto";
                }
            } else {
                $cp = "";
            }
            if ($result['telefono'] != "") {
                if (telefono_valido($result['telefono'])) {
                    $telefono = $result['telefono'];
                } else {
                    $errores[] = "El Teléfono no es correcto";
                }
            } else {
                $telefono = null;
            }

            if ($result['provincia'] != "Seleccione su provincia...") {
                $provincia = $result['provincia'];
            } else {
                $provincia = null;
            }

            if (!isset($errores)) {
                $insertado = editarUser($idAdmin, $email, $password, $nombre, $apellidos, $direccion, $ciudad, $cp, $telefono, $provincia);
                if ($insertado) {
                    header("Location:usuarios.php");
                } else {
                    $errores[] = "Ha habido un error";
                }
            }
        }
        ?>
        <div class="pagina-form">

            <div class="container">
                <div class="tab-content">
                    <?php
                    if (isset($errores)) {
                        ?>
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                <?php
                                foreach ($errores as $error) {
                                    ?>
                                    <li><?php echo $error; ?></li>
                                    <?php
                                }
                                unset($errores);
                                ?>
                            </ul>
                        </div>
                        <?php
                    }
                    ?>
                    <h2>Registrar Usuario</h2>
                    <form role="form" method="POST" action="#">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Introduzca el email" value="<?php if (isset($email)) echo $email; ?>">
                        </div>                       
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" min="8" maxlength="12" id="password" name="password" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control" id="nombre" name="nombre"  placeholder="Introduzca el nombre" value="<?php if (isset($nombre)) echo $nombre; ?>">
                        </div>
                        <div class="form-group">
                            <label for="apellidos">Apellidos</label>
                            <input type="text" class="form-control" id="apellidos" name="apellidos"  placeholder="Introduzca los apellidos"value="<?php if (isset($apellidos)) echo $apellidos; ?>">
                        </div>
                        <div class="form-group">
                            <label for="telefono">Teléfono</label>
                            <input type="tel" min="9" class="form-control" id="telefono" name="telefono"  placeholder="Introduzca el teléfono" value="<?php if (isset($telefono)) echo $telefono; ?>">
                        </div>
                        <div class="form-group">
                            <label for="direccion">Dirección</label>
                            <input type="text" class="form-control" id="direccion" name="direccion"  placeholder="Introduzca la dirección" value="<?php if (isset($direccion)) echo $direccion; ?>">
                        </div>                      
                        <div class="form-group">
                            <div class="col-xs-3">

                                <label for="provincia">Provincia</label>
                                <select class="form-control input-sm" name="provincia" id="provincia" onChange="return provinciaListOnChange()">
                                    <option></option>
                                    <?php
                                    $xml = simplexml_load_file('../provincias/provinciasypoblaciones.xml');

                                    $result = $xml->xpath("/lista/provincia/nombre | /lista/provincia/@id");

                                    for ($i = 0; $i < count($result); $i+=2) {
                                        $e = $i + 1;
                                        $resprovincia = $result[$e];

                                        if ($result[$i] == $provincia) {
                                            echo("<option value='$result[$i]' selected>$resprovincia</option>");
                                        } else {
                                            echo("<option value='$result[$i]'>$resprovincia</option>");
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-xs-3">
                                <label for="localidad">Localidad</label>
                                <select class="form-control input-sm" name="localidad" id="municipio" >
                                    <option selected ><?php echo $ciudad ?></option>
                                </select> 
                            </div>
                            <span id="advice"> </span>
                            <div class="col-xs-3">
                                <label for="cp">CP</label>
                                <input type="text" class="form-control input-sm" id="cp" name="cp"  placeholder="Introduzca el CP" value="<?php if (isset($cp)) echo $cp; ?>">
                            </div>
                        </div>
                        <br/>
                        <button type="submit" name="guardar" class="btn btn-primary">Guardar</button>
                        <button type="button" class="btn btn-primary"  onClick="window.location.href = 'usuarios.php';" >Volver</button>
                    </form>
                </div>

            </div>
        </div>
<?php
include 'pie.php';
?>
        <script src="../js/AjaxCode.js"></script>
        <script src="../js/jquery-1.11.1.min.js"></script>
        <script src="../js/bootstrap.js"></script>
    </body>
</html>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_end_flush();
