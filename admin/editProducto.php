<!DOCTYPE html>
<?php
session_start();
if ($_SESSION["logueado"] == True) {
    $idUser = $_SESSION["email"];
} else {
    session_abort();
    header("Location:loginAdmin.php");
}
?>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_start();
?>
<html lang="es">
    <head>

        <meta charset="UTF8">
        <title>UPOShop</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">
        <script language="javascript">
            function agregarFoto() {
                var tbody = document.getElementById("dataTable");
                var foto = document.createElement("input");
                foto.setAttribute("name", "foto[]");
                foto.setAttribute("class", "form-control");
                foto.setAttribute("type", "file");
                var tr = document.createElement("tr");
                var td = document.createElement("td");
                td.appendChild(foto);
                tr.appendChild(td);
                tbody.appendChild(tr);
            }
            function quitarFoto() {
                var tbody = document.getElementById("dataTable");

                if (tbody.children.length > 2) {
                    tbody.removeChild(tbody.lastChild);
                }

            }
            
        </script>
    </head>
    <body>
        <?php
        include 'cabeceraAdmin.php';
        include '../modelos/tipos.php';
        include '../modelos/productos.php';
        if (isset($_GET['id'])) {
            $idProducto = $_GET['id'];
            $producto = consultarProducto($idProducto);
            $nombre = $producto['nombre'];
            $caracteristicas = $producto['caracteristicas'];
            $precio = $producto['precio'];
            $stock = $producto['stock'];
            $categ = $producto['idTipo'];
            $prov = $producto['idProveedor'];
        }
        
        if (isset($_POST['guardar'])) {
            $filtros = Array(
                'nombre' => FILTER_SANITIZE_MAGIC_QUOTES,
                'caracteristicas' => FILTER_SANITIZE_MAGIC_QUOTES,
                'precio' => FILTER_SANITIZE_MAGIC_QUOTES,
                'stock' => FILTER_SANITIZE_MAGIC_QUOTES,
                'idProveedor' => FILTER_SANITIZE_MAGIC_QUOTES,
                'idTipo' => FILTER_SANITIZE_MAGIC_QUOTES
            );
            $result = filter_input_array(INPUT_POST, $filtros);

            $idTipo = $result['idTipo'];
            $idProveedor = $result['idProveedor'];

            if (!is_numeric($result['nombre']) && $result['nombre'] != "") {
                $nombre = $result['nombre'];
                $nombre = str_replace('"', "", $nombre);
                $nombre = str_replace('/', "", $nombre);
                $nombre = str_replace("'", "", $nombre);
            } else {
                $errores[] = "El nombre es obligatorio";
            }
            if (!is_numeric($result['caracteristicas']) && $result['caracteristicas'] != "") {
                $caracteristicas = $result['caracteristicas'];
            } else {
                $errores[] = "Las caracteristicas es obligatorio";
            }
            if (is_numeric($result['precio']) && $result['precio'] != "") {
                $precio = $result['precio'];
            } else {
                $errores[] = "el precio es obligatorio";
            }
            if (is_numeric($result['stock']) && $result['stock'] != "") {
                $stock = $result['stock'];
            } else {
                $errores[] = "el stock debe de ser un numero positivo o cero";
            }
            
            if (!isset($errores)) {
                $insertado = editarProducto($idProducto, $idTipo, $nombre, $caracteristicas, $precio, $stock, $idProveedor);
                
                if ($insertado) {

                    $fotosTmp = $_FILES['foto']['tmp_name'];
                    $fotosName = $_FILES['foto']['name'];
                    $fotosType = $_FILES['foto']['type'];
                    $fotosSize = $_FILES['foto']['size'];
                    $fotosError = $_FILES['foto']['error'];

                    for ($index = 0; $index < count($fotosName); $index++) {
//                        echo $fotosName[$index] . "<br>";
//                        echo $fotosTmp[$index] . "<br>";
//                        echo $fotosType[$index] . "<br>";
//                        echo $fotosSize[$index] . "<br>";
//                        echo $fotosName[$index] . "<br>";
//                        echo $fotosError[$index] . "<br>" . "<br>";

                        if ($fotosName[$index] != "") {
                            if ($fotosError[$index] > 0) {
                                errorFichero($fotosError[$index]);
                            } else {
                                if (formatoImagen($fotosType[$index]) == true) {
                                    if (($fotosSize[$index] / 1024) > 1024) {
                                        $tamanio = ($fotosSize[$index] / 1024) / 1024;
                                        $erroresFoto[] = "<p><strong>Error: El tama&#241;o de la imagen (" . $tamanio . " Mb) supera 1Mb</strong></p>";
                                    } else {
                                        $tipo = explode("/", $fotosType[$index]);
                                        $sello = time();
                                        $nombreProducto = $_POST['nombre'] . $index;
                                        //$carpetaUsuario = $_POST['nombre'];
                                        $nomImage = $nombreProducto . $sello . "." . $tipo[1];
                                        $hash = md5_file($fotosTmp[$index]);
                                    }
                                } else {
                                    $erroresFoto[] = "<p><strong>Formato de imagen no soportado</strong></p>";
                                }
                            }
                        } else {
                            //$erroresFoto[] = "<p><strong>No se ha subido ning&#250;na imagen</strong></p>";
                            //Aqui no seria error, ya que el producto al crearlo si lo hemos obligado a que tenga 
                            //minimo una foto.
                        }

                        if (!isset($erroresFoto) && $fotosName[$index] != "") {
                            move_uploaded_file($fotosTmp[$index], "../img/" . $nomImage);
                            $productos = ObtenerIdProducto($nombre);
                            if (count($productos) > 1) {
                                foreach ($productos as $producto) {
                                    if (isset($producto['idProducto'])) {
                                        $resultadoId = $producto['idProducto'];
                                        guardarFoto($resultadoId, $nomImage, $hash);
                                    }
                                }
                            }
                        } else {
                            ?>
                            }
                            <?php foreach ($erroresFoto as $error) {
                                ?>
                            <li><?php echo $error; ?></li>
                            <?php
                        }
                        ?>
                        <?php
                    }
                }if (!isset($erroresFoto)) {
                    header("Location:productos.php");
                }
            } else {
                $errores[] = "Ha habido un error: Compruebe de que el nombre, las caracteristicas y el proveedor del producto estan rellenos.";
            }
        }
    }
    if (isset($_GET['action'])) {
        switch ($_GET['action']) {
            case 'eliminar':
                $fotoprincipal = listarFotoIdFoto($_GET['idFoto']);
                $enlace = $fotoprincipal['enlace'];
                echo $fotoprincipal[0];
                echo $enlace;
                unlink("../img/" . $enlace);
                eliminarFoto($_GET['idFoto']);
                $refresh = 'Location: editProducto.php?id=' . $idProducto;
                header($refresh);
        }
    }
    ?>
    <div class="pagina-producto">
        <div class="container">
            <div class="tab-content">
                <br/>
                <?php
                if (isset($errores)) {
                    ?>
                    <div class="alert alert-danger">
                        <ul>
                            <?php
                            foreach ($errores as $error) {
                                ?>
                                <li><?php echo $error; ?></li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                    <?php
                }
                ?>
                <br/>

                <h2>Editar Producto</h2>
                <form role="form" method="POST" action="#" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" id="nombre" name="nombre"  placeholder="Introduzca el nombre" value="<?php if (isset($nombre)) echo $nombre; ?>">
                    </div>
                    <div class="form-group">
                        <label for="caracteristicas">Caracteristicas</label>
                        <textarea rows="7" type="text" class="form-control" id="caracteristicas" name="caracteristicas"  placeholder="Introduzca las caracteristicas"><?php if (isset($caracteristicas)) echo $caracteristicas; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="precio">Precio</label>
                        <input type="number" step="0.01" class="form-control" id="precio" name="precio"  placeholder="Introduzca el precio" value="<?php if (isset($precio)) echo $precio; ?>">
                    </div>                     
                    <div class="form-group">
                        <label for="stock">Stock</label>
                        <input type="number" class="form-control" id="stock" name="stock"  placeholder="Introduzca unidades del producto en stock" value="<?php if (isset($stock)) echo $stock; ?>" >
                    </div>
                    <div class="form-group">
                        <label for="idTipo">Categoría</label>
                        <select class="form-control" name="idTipo">
                            <?php
                            $tipos = consultarCategorias();
                            $isPadre = true;
                            if (count($tipos) > 1) {
                                foreach ($tipos as $tipo) {
                                    if (isset($tipo['idTipo'])) {
                                        $categoria = $tipo['nombre'];
                                        if ($tipo['idPadre'] > 0) {
                                            $padre = consultarPadre($tipo['idPadre']);
                                            echo $padre[0]['nombre'];
                                            $categoria = $padre[0]['nombre'] . " > " . $categoria;
                                            while ($padre[0]['idPadre'] > 0) {
                                                $padre = consultarPadre($padre[0]['idPadre']);
                                                $categoria = $padre[0]['nombre'] . " > " . $categoria;
                                            }
                                        } else {
                                            $isPadre = true;
                                        }
                                        if ($categ != $tipo['idTipo']) {
                                            if ($isPadre) {
                                                ?>
                                                <optgroup  label="<?php echo $categoria ?>"</optgroup> 
                                                <?php
                                                $isPadre = false;
                                            } else {
                                                ?>
                                                <option value="<?php echo $tipo['idTipo'] ?>"><?php echo $categoria ?></option>
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <option value="<?php echo $tipo['idTipo'] ?>"><?php echo $categoria ?></option>
                                            <?php
                                        }
                                    }
                                    $categoria = "";
                                }
                            } else {
                                ?>
                                <div class="alert alert-warning" role="alert">No existen categorias. Por favor, se necesita crear primero la categoria del producto.</div>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div style="width: 60%"class="form-group">
                        <label for="idProveedor">Proveedor</label>
                        <select class="form-control" name="idProveedor">
                            <?php
                            $proveedores = consultarProveedores();
                            if (count($proveedores) > 1) {
                                foreach ($proveedores as $proveedor) {
                                    if (isset($proveedor['idProveedor'])) {
                                        if ($proveedor['idProveedor'] != $prov) {
                                            ?>
                                            <option value="<?php echo $proveedor['idProveedor'] ?>"><?php echo $proveedor['nombre'] ?></option> 
                                            <?php
                                        } else {
                                            ?>
                                            <option selected value="<?php echo $proveedor['idProveedor'] ?>"><?php echo $proveedor['nombre'] ?></option> 
                                            <?php
                                        }
                                    }
                                }
                            } else {
                                ?>
                                <div class="alert alert-warning" role="alert">No existen proveedores. Por favor, se necesita crear primero el proveedor del producto.</div>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div  class="form-group">
                        <?php
                        $fotos = listarFotos($idProducto);
                        $cont = 0;
                        if (count($fotos) > 1) {
                            foreach ($fotos as $foto) {
                                $cont++;
                                //print_r($foto);
                                if (isset($foto['enlace'])) {
                                    $link = $foto['enlace'];
                                    echo "<img style='width: 15%; padding: 1%' src='../img/" . $link . "' >";
                                    ?>
                                    <a href="?action=eliminar&idFoto=<?php echo $foto['idFoto'] ?>&id=<?php echo $idProducto?>" <span class='glyphicon glyphicon-trash'></span> Borrar</a>
                                 <?php   echo "</div>";
                                }
                            }
                        }
                        ?>
                    </div>
                    <div>
                        <table>
                            <tbody id="dataTable">
                                <tr>
                                    <td><label>Fotograf&iacute;as</label></td>
                                    <td><input type="button" class="btn btn-primary" onclick="agregarFoto()" value="+"/>
                                        <input type="button" class="btn btn-primary" onclick="quitarFoto()" value="-" />
                                    </td>
                                </tr>
                                <tr>
                                    <td><input style="" class="form-control" name="foto[]" type="file" /></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <button type="submit" name="guardar" class="btn btn-primary">Guardar</button>
                    <button type="button" class="btn btn-success" onClick="window.location.href = './productos.php';" >Volver</button>

                </form>
                <br/>
            </div>
        </div>
    </div>
    <?php
    include 'pie.php';
    ?>
    <script src="./js/jquery-1.11.1.min.js"></script>
    <script src="./js/bootstrap.js"></script>
</body>
</html>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_end_flush();
