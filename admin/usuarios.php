<?php
session_start();
if ($_SESSION["logueado"] == True) {
    $idUser = $_SESSION["email"];
} else {
    session_abort();
    header("Location:loginAdmin.php");
}
?>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_start();
?>
<!DOCTYPE html>
<html lang="es">
    <head>

        <meta charset="UTF-8">
        <title>UPOShop</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">
        <link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.css">
    </head>
    <body>
        <?php
        include 'cabeceraAdmin.php';
        include '../modelos/usuarios.php';
        $limit_end = 15;

        if (isset($_GET['pos'])) {
            $ini = $_GET['pos'];
        } else {
            $ini = 1;
        }
        $init = ($ini - 1) * $limit_end;
        $url = basename($_SERVER ["PHP_SELF"]);

        if (isset($_GET['action'])) {
            switch ($_GET['action']) {
                case 'eliminar':
                    $eliminado = eliminarUser($_GET['id']);
                    if ($eliminado) {
                        header('Location: usuarios.php');
                    } else {
                        ?>
                        <div class="alert alert-danger" role="alert">no se ha podido eliminar</div>
                        <?php
                    }

                    break;

                case 'editar':
                    header("Location: editUsuario.php?id=" . $_GET['id'] . "");
                    break;
            }
        }
        ?>

        <!-- Tab panes -->
        <div class="pagina-producto">
            <div class="container">
                <div class="tab-content">
                    <div class="tab-pane active">
                        <br/>
                        <p><button type="button" class="btn btn-primary"  onClick="window.location.href = './insertUsuario.php';"><span class="glyphicon glyphicon-plus"></span> Añadir</button></p>
                        <br/>
                        <?php
                        
                        $usuarios = consultarUsuarios($ini, $limit_end);
                        $total = ceil(count($usuarios) / $limit_end);
                        
                        ?>
                        <div class="table-responsive">
                            <table id="example" cellspacing="0" width="100%" class="table table-hover">
                                <caption><h2>Usuarios</h2></caption>	
                                <thead>
                                    <tr>
                                        <th>Email</th>
                                        <th>Nombre</th>
                                        <th>Apellidos</th>
                                        <th>Dirección</th>
                                        <th>CP</th>
                                        <th>Teléfono</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    //var_dump($usuarios);
                                    if (count($usuarios) > 1) {
                                        foreach ($usuarios as $usuario) {
                                            if (isset($usuario['email'])) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $usuario['email'] ?></td>
                                                    <td><?php echo $usuario['nombre'] ?></td>
                                                    <td><?php echo $usuario['apellidos'] ?></td>
                                                    <td><?php echo $usuario['direccion'] ?></td>
                                                    <td><?php echo $usuario['cp'] ?></td>
                                                    <td><?php echo $usuario['telefono'] ?></td>
                                                    <td>
                                                        <a href="?action=editar&id=<?php echo $usuario['email']; ?>"><span class="glyphicon glyphicon-edit"></span> Editar</a>
                                                        <a href="?action=eliminar&id=<?php echo $usuario['email']; ?>"><span class="glyphicon glyphicon-trash"></span> Borrar</a>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                    } else {
                                        ?>
                                    <div class="alert alert-warning" role="alert">No existen usuarios <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button></div>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <div >
                            <ul class="pagination">
                                <?php
                                if (($ini - 1) == 0) {
                                    ?>
                                    <li class="disabled"><a href="#">&laquo;</a></li>
                                        <?php
                                    } else {
                                        echo "<li><a href='$url?pos=" . ($ini - 1) . "'><b>&laquo;</b></a></li>";
                                    }
                                    for ($k = 1; $k <= $total; $k++) {
                                        if ($ini == $k) {
                                            echo "<li><a href='#'><b>" . $k . "</b></a></li>";
                                        } else {
                                            echo "<li><a href='$url?pos=$k'>" . $k . "</a></li>";
                                        }
                                    }
                                    if ($ini == $total) {
                                        echo "<li><a href='#'>&raquo;</a></li>";
                                    } else {
                                        echo "<li><a href='$url?pos=" . ($ini + 1) . "'><b>&raquo;</b></a></li>";
                                    }
                                    ?>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <?php
        include './pie.php';
        ?>
        <script src="../js/jquery-1.11.1.min.js"></script>
        <script src="../js/bootstrap.js"></script>
    </body>
</html>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_end_flush();
