     <header class="cabecera">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="menuUsuario">
                            <ul>
                                <!--<li><a href="#"><i class="fa fa-user"></i> My Account</a></li>
                                <li><a href="#"><i class="fa fa-user"></i> My Cart</a></li>
                                <li><a href="checkout.html"><i class="fa fa-user"></i> Checkout</a></li>!-->
                                <li><a href="logout.php"><i class="fa fa-user"></i> Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header> 
        <div class="fondocorp">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="logo">
                            <h1><a href="index.php">U<span>POShop</span></a></h1>
                        </div>
                    </div>

                    
                </div>
            </div>
        </div> <!-- End site branding area -->
        <!--Menu principal head-->
        <nav id="menucompras" class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" >
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">UPOShop</a>
                </div>
                
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="usuarios.php">Usuarios</a></li>
                        <li><a href="compras.php">Compras</a></li>
                        <li><a href="tiposProductos.php">Tipos de Productos</a></li>
                        <li><a href="productos.php">Productos</a></li>
                        <li><a href="ofertas.php">Ofertas</a></li>
                        <li><a href="opiniones.php">Opiniones</a></li>
                        <li><a href="proveedores.php">Proveedores</a></li>
                        <li><a href="index.php">Administradores</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>	
