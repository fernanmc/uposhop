<?php
session_start();
if ($_SESSION["logueado"] == True) {
    $idUser = $_SESSION["email"];
} else {
    session_abort();
    header("Location:loginAdmin.php");
}
?>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_start();
?>

<!DOCTYPE html>
<html lang="es">
    <head>

        <meta charset="UTF-8">
        <title>UPOShop</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">
        <link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.css">
        
    </head>
    <body>
        <?php
        include 'cabeceraAdmin.php';
        include '../modelos/ofertas.php';
        include '../modelos/tipos.php';

        if (isset($_GET['id'])) {
            $idOferta = $_GET['id'];
            $oferta = consultarOferta($idOferta);
            $nombre = $oferta['nombre'];
            $fechaInicio = date("Y-m-d", strtotime($oferta['fechaInicio']));
            $fechaFin = date("Y-m-d", strtotime($oferta['fechaFin']));
//            $fechaInicio=$oferta['fechaInicio'];
//            $fechaFin=$oferta['fechaFin'];
            $descuento = $oferta['descuento'];
            $cupon = $oferta['cupon'];
            $idTipo=$oferta["idTipo"];
        }

        if (isset($_POST['guardar'])) {
            $filtros = Array(
                
                'nombre' => FILTER_SANITIZE_MAGIC_QUOTES,
                'fechaInicio' => FILTER_SANITIZE_MAGIC_QUOTES,
                'fechaFin' => FILTER_SANITIZE_MAGIC_QUOTES,
                'descuento' => FILTER_SANITIZE_NUMBER_INT,
                'cupon' => FILTER_SANITIZE_MAGIC_QUOTES,
                'idTipo'=> FILTER_SANITIZE_MAGIC_QUOTES,
            );
            $result = filter_input_array(INPUT_POST, $filtros);

            if (!is_numeric($result['nombre']) && $result['nombre'] != "") {
                $nombre = $result['nombre'];
            } else {
                $errores[] = "El nombre es obligatorio";
            }
            if ($result['fechaInicio'] < $result['fechaFin']) {
                $fechaInicio = date("Y-m-d", strtotime($result['fechaInicio']));
                $fechaFin = date("Y-m-d", strtotime($result['fechaFin']));
            } else {
                $errores[] = "La fecha de inicio debe ser anterior a la de fin y son obligatorias";
            }
            if (is_numeric($result['descuento']) && $result['descuento'] != "") {
                $descuento = $result['descuento'];
            } else {
                $errores[] = "El descuento debe ser numero y es obligatorio";
            }
            if (!is_numeric($result['cupon']) && $result['cupon'] != "") {
                $cupon = $result['cupon'];
            } else {
                $errores[] = "El cupón es obligatorio";
            }
             if ( $result['idTipo'] != "") {
                $idTipo = $result['idTipo'];
            } else {
                $errores[] = "El cupón es obligatorio";
            }
            

            if (!isset($errores)) {
                $insertado = editarOferta($idOferta, $nombre, $fechaInicio, $fechaFin, $descuento, $cupon);
                if ($insertado) {
                    header("Location:ofertas.php");
                } else {
                    $errores[] = "Ha habido un error";
                }
            }
        }
        ?>
        <div class="pagina-producto">
            <div class="container">
                <div class="tab-content">
                    <br/>
                    <?php
                    if (isset($errores)) {
                        ?>
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                <?php
                                foreach ($errores as $error) {
                                    ?>
                                    <li><?php echo $error; ?></li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                        <?php
                    }
                    ?>
                    <br/>
                    <h2>Editar Oferta</h2>
                    <form role="form" method="POST" action="#">
                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control" id="nombre" name="nombre"  placeholder="Introduzca el nombre" value="<?php echo $nombre ?>">
                        </div>                     
                        <div class="form-group">
                            <label for="fechaInicio">Fecha de inicio</label>
                            <input type="date" class="form-control" id="fechaInicio" name="fechaInicio"  placeholder="Introduzca la fecha de inicio" value="<?php echo $fechaInicio ?>">
                        </div>
                        <div class="form-group">
                            <label for="fechaFin">Fecha de fin</label>
                            <input type="date" class="form-control" id="fechaFin" name="fechaFin"  placeholder="Introduzca la fecha de fin" value="<?php echo $fechaFin ?>">
                        </div>
                        <div class="form-group">
                            <label for="descuento">Descuento</label>
                            <input type="number" class="form-control" id="descuento" name="descuento"  placeholder="Introduzca el porcentaje de descuento" value="<?php echo $descuento ?>">
                        </div>
                        <div class="form-group">
                            <label for="nombre">Cup&oacute;n</label>
                            <!--<input type="button" class="btn btn-primary" onclick="generarCupon()" value="Generar Cupon"/>-->
                            <input type="text" class="form-control" id="cupon" name="cupon"  placeholder="Genere cupon" value="<?php echo $cupon ?>">
                        </div> 
                         <?php
                         $tipos=consultarTipos();           
                         ?>
                        <div class="form-group">
                            <label for="idTipo">Tipo Producto</label>
                            <select class="form-control" id="idTipo" name="idTipo">
                                <option value="0" selected >Tipo producto</option>
                                <?php
                                       if (count($tipos) > 1) {
                                        foreach ($tipos as $tipo) {
                                            if(isset($tipo['nombre'])){
                                            if($idTipo==$tipo["idTipo"]){
                                ?>
                                             <option  value="<?php echo $tipo['idTipo']; ?>" selected><?php echo $tipo['nombre']; ?></option>      
                                <?php    
                                            }else{
                                ?>
                                             <option  value="<?php echo $tipo['idTipo']; ?>" ><?php echo $tipo['nombre']; ?></option>
                                <?php
                                            }
                                            }
                                         }
                                      }
                                ?>
                            </select>
                        </div>
                        <button type="submit" name="guardar" class="btn btn-primary">Guardar</button>
                        <button type="button" class="btn btn-success" onClick="window.location.href = 'index.php';" >Volver</button>
                    </form>
                    <br/>
                </div>
            </div>
        </div>
        <?php
        include 'pie.php';
        ?>
        <script src="../js/jquery-1.11.1.min.js"></script>
        <script src="../js/bootstrap.js"></script>
    </body>
</html>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_end_flush();

