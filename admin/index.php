<?php
session_start();
if($_SESSION["logueado"]==True){
    $idUser=$_SESSION["email"];
}else{
    session_abort();
    header("Location:loginAdmin.php");
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>

        <meta charset="UTF-8">
        <title>UPOShop</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">
        <link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.css">
        
    </head>
    <body>
        <?php
        //  require_once './modelos/conectar.php';
        include './cabeceraAdmin.php';
        include '../modelos/Administradores.php';
        if (isset($_GET['action'])) {
            switch ($_GET['action']) {
                case 'eliminar':
                    echo "Dentro";
                    $eliminado = Administradores::eliminarAdmin($_GET['id']);
                    if ($eliminado) {
                        header('Location: index.php');
                    } else {
                        ?>
                        <div class="alert alert-danger" role="alert">no se ha podido eliminar</div>
                    <?php
                    }

                    break;

                case 'editar':
                    header("Location: editAdmin.php?id=" . $_GET['id'] . "");
                    break;
            }
        }
        ?>

        <!-- Tab panes -->
        <div class="pagina-producto">
            <div class="container">
                <div class="tab-content">
                    <div class="tab-pane active">
                        <br/>
                        <p><button type="button" class="btn btn-primary"  onClick="window.location.href='./insertAdmin.php';"><span class="glyphicon glyphicon-plus"></span> Añadir</button></p>
                        <br/>
                        <div class="table-responsive">
                            <table id="example" cellspacing="0" width="100%" class="table table-hover">
                                <caption><h2>Administradores</h2></caption>	
                                <thead>
                                    <tr>
                                        <th>Email</th>
                                        <th>Nombre</th>
                                        <th>Apellidos</th>
                                        <th>Fecha Registro</th>
                                        <th>Fecha Actualización</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $administradores = Administradores::consultarAdministradores();
                                    //var_dump($administradores);
                                    if (count($administradores) > 1) {
                                        foreach ($administradores as $administrador) {
                                            if (isset($administrador['email'])) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $administrador['email'] ?></td>
                                                    <td><?php echo $administrador['Nombre'] ?></td>
                                                    <td><?php echo $administrador['apellidos'] ?></td>
                                                    <td><?php echo $administrador['fecharegistro'] ?></td>
                                                    <td><?php echo $administrador['fechaupdate'] ?></td>
                                                    <td>
                                                        <a href="?action=editar&id=<?php echo $administrador['email']; ?>"><span class="glyphicon glyphicon-edit"></span> Editar</a>
                                                        <a href="?action=eliminar&id=<?php echo $administrador['email']; ?>"><span class="glyphicon glyphicon-trash"></span> Borrar</a>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                    } else {
                                        ?>
                                    <div class="alert alert-warning" role="alert">No existen administradores <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button></div>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <ul class="pagination">
                            <li class="disabled"><a href="#">&laquo;</a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">&raquo;</a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
        <?php
        include './pie.php';
        ?>
        <script src="../js/jquery-1.11.1.min.js"></script>
        <script src="../js/bootstrap.js"></script>
    </body>
</html>