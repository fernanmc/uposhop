<?php
session_start();
if ($_SESSION["logueado"] == True) {
    $idUser = $_SESSION["email"];
} else {
    session_abort();
    header("Location:loginAdmin.php");
}
?>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_start();
?>
<!DOCTYPE html>
<html lang="es">
    <head>

        <meta charset="UTF-8">
        <title>UPOShop</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">
        <link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.css">
    </head>
    <body>
        <?php
        //  require_once './modelos/conectar.php';
        include '../admin/cabeceraAdmin.php';
        include '../modelos/productos.php';

        if (isset($_GET['action'])) {
            switch ($_GET['action']) {
                case 'eliminar':
                    echo "Dentro";
                    $productos = ObtenerIdNombreFoto($_GET['id']);
                    if ($productos) {
                        if (count($productos) > 1) {
                            foreach ($productos as $producto) {
                                if (isset($producto['idProducto'])) {
                                    $enlace = $producto['enlace'];
                                    unlink("../img/" . $enlace);
                                }
                            }
                        }
                        $eliminado = eliminarProducto($_GET['id']);

                        header('Location: ../admin/productos.php');
                    } else {
                        ?> 
                        <div class="alert alert-danger" role="alert">No se ha podido eliminar</div>
                        <?php
                    }

                    break;
                case 'editar':
                    header("Location: editProducto.php?id=" . $_GET['id'] . "");
                    break;
            }
        }
        ?>

        <!-- Tab panes -->
        <div class="pagina-producto">
            <div class="container">
                <div class="tab-content">
                    <div class="tab-pane active">
                        <br/>
                        <p><button type="button" class="btn btn-primary"  onClick="window.location.href = '../admin/insertProducto.php';"><span class="glyphicon glyphicon-plus"></span> Añadir</button></p>
                        <br/>
                        <div class="table-responsive">
                            <table id="example" cellspacing="0" width="100%" class="table table-hover">
                                <caption><h2>Productos</h2></caption>	
                                <thead>
                                    <tr>
                                        <th nowrap>ID Producto</th>
                                        <th nowrap>Nombre</th>
                                        <th nowrap>Caracteristicas</th>
                                        <th nowrap>Precio</th>
                                        <th nowrap>Stock</th>
                                        <th nowrap>Proveedor</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $productos = consultarProductos();
                                    //var_dump($administradores);
                                    if (count($productos) > 1) {
                                        foreach ($productos as $producto) {
                                            if (isset($producto['idProducto'])) {
                                                ?>
                                                <tr>
                                                    <td nowrap><?php echo $producto['idProducto'] ?></td>
                                                    <td nowrap><?php echo $producto['nombre'] ?></td>
                                                    <td><?php echo $producto['caracteristicas'] ?></td>
                                                    <td nowrap><?php echo $producto['precio'] ?></td>
                                                    <td nowrap><?php echo $producto['stock'] ?></td>
                                                    <?php
                                                    $proveedores = consultarProveedores();
                                                    if (count($proveedores) > 1) {
                                                        foreach ($proveedores as $proveedor) {
                                                            if (isset($proveedor['idProveedor'])) {
                                                                if ($proveedor['idProveedor'] == $producto['idProveedor']) {
                                                                    ?>
                                                                    <td><?php echo $proveedor['nombre'] ?></td>
                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                    }
                                                    ?>

                                                    <td nowrap>
                                                        <a href="?action=editar&id=<?php echo $producto['idProducto']; ?>"><span class="glyphicon glyphicon-edit"></span> Editar</a>
                                                        <a href="?action=eliminar&id=<?php echo $producto['idProducto']; ?>"><span class="glyphicon glyphicon-trash"></span> Borrar</a>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                    } else {
                                        ?>
                                    <div class="alert alert-warning" role="alert">No existen productos <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button></div>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <ul class="pagination">
                            <li class="disabled"><a href="#">&laquo;</a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">&raquo;</a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
        <?php
        include '../admin/pie.php';
        ?>
        <script src="../js/jquery-1.11.1.min.js"></script>
        <script src="../js/bootstrap.js"></script>
    </body>
</html>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_end_flush();
