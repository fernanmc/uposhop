<?php
session_start();
if($_SESSION["logueado"]==True){
    $idUser=$_SESSION["email"];
}else{
    session_abort();
    header("Location:loginAdmin.php");
}
?>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_start();
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>UPOShop</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">
        <link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.css">
    </head>
    <body>

        <?php
        //  require_once './modelos/conectar.php';
        include './cabeceraAdmin.php';
        include '../modelos/opiniones.php';

        if (isset($_GET['action'])) {
            switch ($_GET['action']) {
                case 'eliminar':
                    $eliminado = eliminarOpinion($_GET['id']);
                    if ($eliminado) {
                        header('Location: opiniones.php');
                    } else {
                        ?>
                        <div class="alert alert-danger" role="alert">no se ha podido eliminar</div>
                        <?php
                    }

                    break;
            }
        }
        ?>

        <!-- Tab panes -->
        <div class="pagina-proveedor">
            <div class="container">
                <div class="tab-content">
                    <div class="tab-pane active">
                        <br/>
                        <div class="table-responsive">
                            <table id="example" cellspacing="0" width="100%" class="table table-hover">
                                <caption><h2>Opiniones</h2></caption>	
                                <thead>
                                    <tr>
                                        <th>Usuario</th>
                                        <th>Fecha publicacion</th>
                                        <th>Comentario</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $opiniones = consultarOpinionesAdmin();
                                    //var_dump($administradores);
                                    if (count($opiniones) > 1) {
                                        foreach ($opiniones as $opinion) {
                                            if (isset($opinion['idOpinion'])) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $opinion['idUsuario'] ?></td>
                                                    <td><?php echo $opinion['fecha'] ?></td>
                                                    <td><?php echo $opinion['comentario'] ?></td>
                                                    <td>
                                                        <a href="?action=eliminar&id=<?php echo $opinion['idOpinion']; ?>"><span class="glyphicon glyphicon-trash"></span> Borrar</a>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                    } else {
                                        ?>
                                    <div class="alert alert-warning" role="alert">No existen Opiniones <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button></div>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <ul class="pagination">
                            <li class="disabled"><a href="#">&laquo;</a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">&raquo;</a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
        <?php
        include './pie.php';
        ?>
        <script src="../js/jquery-1.11.1.min.js"></script>
        <script src="../js/bootstrap.js"></script>
    </body>
</html>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_end_flush();