<?php
session_start();
ob_start();
if($_SESSION["logueado"]==True){
    $idUser=$_SESSION["email"];
}else{
    session_abort();
    header("Location:loginAdmin.php");
}
?>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo

?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>UPOShop</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">
        <link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.css">
    </head>
    <body>

        <?php
        //  require_once './modelos/conectar.php';
        include './cabeceraAdmin.php';
        include '../modelos/compras.php';

        if (isset($_GET['action'])) {
            switch ($_GET['action']) {
                case 'eliminar':
                    //echo "Dentro";
                    $eliminado = eliminarCompra($_GET['id']);
                    if ($eliminado) {
                        header('Location: compras.php');
                    } else {
                        ?>
                        <div class="alert alert-danger" role="alert">no se ha podido eliminar</div>
                        <?php
                    }

                    break;
                case 'editar':
                    header("Location: editCompra.php?id=" . $_GET['id'] . "");
                    break;
            }
        }
        ?>

        <!-- Tab panes -->
        <div class="pagina-proveedor">
            <div class="container">
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="table-responsive">
                            <table id="example" cellspacing="0" width="100%" class="table table-hover">
                                <caption><h2>Tus compras</h2></caption>	
                                <thead>
                                    <tr>
                                        <th>Usuario</th>
                                        <th>Nombre</th>
                                        <th>Precio Total</th>
                                        <th>Fecha compra</th>
                                        <th>Estado</th>
                                        <th>Fecha actualizacion</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $compras = consultarComprasTotal();
                                    //var_dump($administradores);
                                    if (count($compras) > 1) {
                                        foreach ($compras as $compra) {
                                            if (isset($compra['idCompra'])) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $compra['email'] ?></td>
                                                    <td><?php echo $compra['nombre']." ".$compra['apellidos'] ?></td>
                                                    <td><?php echo $compra['precioTotal'] ?></td>
                                                    <td><?php echo $compra['fechaRegistro'] ?></td>
                                                    <td><?php echo $compra['estado'] ?></td>
                                                    <td><?php echo $compra['fechaActualizacion'] ?></td>
                                                    <td>
                                                        <a href="?action=editar&id=<?php echo $compra['idCompra']; ?>"><span class="glyphicon glyphicon-edit"></span> Editar</a>
                                                        <a href="?action=eliminar&id=<?php echo $compra['idCompra']; ?>"><span class="glyphicon glyphicon-trash"></span> Borrar</a>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                    } else {
                                        ?>
                                    <div class="alert alert-warning" role="alert">No existen Compras <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button></div>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <ul class="pagination">
                            <li class="disabled"><a href="#">&laquo;</a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">&raquo;</a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
        <?php
        include '../pie.php';
        ?>
        <script src="../js/jquery-1.11.1.min.js"></script>
        <script src="../js/bootstrap.js"></script>
    </body>
</html>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_end_flush();