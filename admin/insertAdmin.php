<?php
session_start();
if($_SESSION["logueado"]==True){
    $idUser=$_SESSION["email"];
}else{
    session_abort();
    header("Location:loginAdmin.php");
}
?>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_start();
?>
<!DOCTYPE html>
<html lang="es">
    <head>

        <meta charset="UTF-8">
        <title>UPOShop</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">
        <link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.css">
        
    </head>
    <body>
        <?php
        include 'cabeceraAdmin.php';
        include '../modelos/Administradores.php';
       
        if(isset($_POST['guardar'])){
            $filtros= Array(
                   'email'=>FILTER_SANITIZE_EMAIL,
                   'password'=>FILTER_SANITIZE_MAGIC_QUOTES,
                   'nombre'=>FILTER_SANITIZE_MAGIC_QUOTES,
                   'apellidos'=>FILTER_SANITIZE_MAGIC_QUOTES
               );
               $result=  filter_input_array(INPUT_POST, $filtros);
               if(!is_numeric($result['email']) && $result['email']!=""){
                   $email=$result['email'];
               }  else {
                   $errores[]="el email es obligatorio";
               }
                if(!is_numeric($result['password']) && $result['password']!="" && strlen($result['password'])>7){
                   $password=$result['password'];
                   $password = password_hash($password, PASSWORD_BCRYPT, ['cost' => 12]);
               }  else {
                   $errores[]="el password debe de ser completo";
               }
                   $nombre=$result['nombre'];
                   $apellidos=$result['apellidos'];
              if(!isset($errores)){
                  $insertado=  Administradores::insertarAdmin($email,$password,$nombre,$apellidos);
                  if($insertado){
                      header("Location:index.php");
                  }else{
                      $errores[]="Ha habido un error";
                  }
              }
        }
        ?>
        <div class="pagina-producto">
            <div class="container">
                <div class="tab-content">
                    <br/>
                  <?php
                  if(isset($errores)){
                  ?>
                    <div class="alert alert-danger" role="alert">
                        <ul>
                            <?php
                           foreach ($errores as $error){
                            ?>
                            <li><?php echo $error; ?></li>
                            <?php
                           }
                            ?>
                        </ul>
                    </div>
                   <?php
                  }
                  ?>
                    <br/>
                    <h2>Registrar Administrador</h2>
                    <form role="form" method="POST" action="#">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Introduzca el email">
                        </div>                       
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" min="8" maxlength="12" id="password" name="password" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control" id="nombre" name="nombre"  placeholder="Introduzca el nombre">
                        </div>
                        <div class="form-group">
                            <label for="apellidos">Apellidos</label>
                            <input type="text" class="form-control" id="apellidos" name="apellidos"  placeholder="Introduzca los apellidos">
                        </div>
                        <button type="submit" name="guardar" class="btn btn-primary">Guardar</button>
                        <button type="button" class="btn btn-success" onClick="window.location.href='index.php';" >Volver</button>
                    </form>
                    <br/>
                </div>
            </div>
        </div>
          <?php
        include 'pie.php';
        ?>
        <script src="../js/jquery-1.11.1.min.js"></script>
        <script src="../js/bootstrap.js"></script>
    </body>
</html>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_end_flush();