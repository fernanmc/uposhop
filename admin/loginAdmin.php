<?php session_start();?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>UPOshop</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">
        <link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.css">
</head>
<body>
	 <?php        
        include '../modelos/Administradores.php';
        if(isset($_POST['login'])){
               
               $filtros= Array(
                   'email'=>FILTER_SANITIZE_MAGIC_QUOTES,
                   'password'=>FILTER_SANITIZE_MAGIC_QUOTES
               );
               $result=  filter_input_array(INPUT_POST, $filtros);
               if(!is_numeric($result['email']) && $result['email']!=""){
                   $email=$result['email'];
               }  else {
                   $errores[]="No ha introducido el email";
               }
                if(!is_numeric($result['password']) && $result['password']!=""){
                   $password=$result['password'];
               }  else {
                   $errores[]="El password debe de ser completo";
               }
               if(!isset($errores)){
                 
                   
                   $existe=  Administradores::existeUsuario($email,$password);
                     if($existe==True){
                         $_SESSION["logueado"]=True;
                         $_SESSION["email"]=$email;
                         header("Location:index.php");
                     }else{
                           $errores[]="EL USUARIO NO EXISTE";
                     }
               }
        }  
 
        
        ?>
    <header class="cabecera">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="menuUsuario">
                           <!-- <ul>
                                <li><a href="#"><i class="fa fa-user"></i> My Account</a></li>
                                <li><a href="cart.html"><i class="fa fa-user"></i> My Cart</a></li>
                                <li><a href="checkout.html"><i class="fa fa-user"></i> Checkout</a></li>
                                <li><a href="#"><i class="fa fa-user"></i> Login</a></li>
                            </ul>!-->
                        </div>
                    </div>
                </div>
            </div>
        </header> 
        <div class="fondocorp">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="logo">
                            <h1><a href="#">U<span>POShop</span></a></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- End site branding area -->
        <!--Menu principal head-->
        <nav id="menucompras" class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" >
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">UPOShop</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                 <!--   <ul class="nav navbar-nav">
                        <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
                        <li><a href="#">Link</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" >Dropdown <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li  class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                                <li  class="divider"></li>
                                <li><a href="#">One more separated link</a></li>
                            </ul>
                        </li>
                    </ul>!-->
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <div class="pagina-login">
            <div class="container">
                <div class="row">
                    <?php
                    if (isset($errores)) {
                        ?>
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                <?php
                                foreach ($errores as $error) {
                                    ?>
                                    <li><?php echo $error; ?></li>
                                    <?php
                                }
                   
                                ?>
                            </ul>
                    <?php } ?>
                        </div>
                    <form class="login" method="POST" action="#">
                            <h2 class="login-heading">Login Administración</h2>
                            <label for="email" class="sr-only">User</label>
                            <input type="email" id="email" name="email" class="form-control" placeholder="Email" value="<?php if(isset($email))echo $email;?>">
                            <label for="inputPassword" class="sr-only">Password</label>
                            <input type="password" id="password" name="password" class="form-control" placeholder="Password">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="remember-me"> Recuerdame
                                </label>
                            </div>
                            <button class="btn btn-lg btn-primary btn-block" type="submit" name="login">Entrar</button>
                      </form>
                </div>
            </div>
        </div>
	<?php
        include './pie.php';
        ?>

            <script src="../js/jquery-1.11.1.min.js"></script>
            <script src="../js/bootstrap.js"></script>
    </body>
</html>
