<?php
session_start();
if($_SESSION["logueado"]==True){
    $idUser=$_SESSION["email"];
}else{
    session_abort();
    header("Location:loginAdmin.php");
}
?>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_start();
?>
<!DOCTYPE html>
<html lang="es">
    <head>

        <meta charset="UTF-8">
        <title>UPOShop</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">
        <link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.css">
        
    </head>
    <body>
        <?php
        include 'cabeceraAdmin.php';
        include '../modelos/tipos.php';
        
        if (isset($_GET['id'])) {
            $idTipo = $_GET['id'];
            $tipo = consultarTipo($idTipo);
            $nombre = $tipo['nombre'];
            $padre = $tipo['idPadre'];
        }
        if (isset($_POST['guardar'])) {
            $filtros = Array(
                'nombre' => FILTER_SANITIZE_MAGIC_QUOTES,
                'idPadre' => FILTER_SANITIZE_NUMBER_INT
            );
            $result = filter_input_array(INPUT_POST, $filtros);
            

            $nombre = $result['nombre'];
            $idPadre = $result['idPadre'];
            if (!isset($errores)) {
                $insertado = editarTipo($idTipo, $nombre, $idPadre);
                if ($insertado) {
                    header("Location:tiposProductos.php");
                } else {
                    $errores[] = "Ha habido un error";
                }
            }
        }
        ?>
        <div class="pagina-producto">
            <div class="container">
                <div class="tab-content">
                    <br/>
                    <?php
                    if (isset($errores)) {
                        ?>
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                <?php
                                foreach ($errores as $error) {
                                    ?>
                                    <li><?php echo $error; ?></li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                        <?php
                    }
                    ?>
                    <br/>
                    <h2>Editar Tipo producto</h2>
                    <form role="form" method="POST" action="#">
                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control" id="nombre" name="nombre"  placeholder="Introduzca el nombre" value="<?php echo $nombre ?>">
                        </div>                     
                        <div class="form-group">
                            <?php
                            $tipos=consultarTiposPadres();
                            ?>
                            <label for="idPadre">Tipo Padre</label>
                            <select class="form-control" id="idPadre" name="idPadre">
                                <option value="0" >Tipo Padre</option>
                                <?php
                                       if (count($tipos) > 1) {
                                        foreach ($tipos as $tipo) {
                                            if(isset($tipo['nombre'])){
                                               if($padre==$tipo['idTipo']){
                                ?>
                                                <option value="<?php echo $tipo['idTipo']; ?>" selected ><?php echo $tipo['nombre']; ?></option>      
                                 <?php       }else{?>
                                                <option value="<?php echo $tipo['idTipo']; ?>"  ><?php echo $tipo['nombre']; ?></option>
                                 <?php          }
                                            }
                                         }
                                      }
                                ?>
                            </select>
                        </div>
                        
                        <button type="submit" name="guardar" class="btn btn-primary">Guardar</button>
                        <button type="button" class="btn btn-success" onClick="window.location.href='index.php';" >Volver</button>
                    </form>
                    <br/>
                </div>
            </div>
        </div>
        <?php
        include 'pie.php';
        ?>
        <script src="../js/jquery-1.11.1.min.js"></script>
        <script src="../js/bootstrap.js"></script>
    </body>
</html>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_end_flush();

