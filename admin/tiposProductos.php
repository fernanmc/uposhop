<?php
session_start();
if ($_SESSION["logueado"] == True) {
    $idUser = $_SESSION["email"];
} else {
    session_abort();
    header("Location:loginAdmin.php");
}
?>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_start();
?>
<!DOCTYPE html>
<html lang="es">
    <head>

        <meta charset="UTF-8">
        <title>UPOShop</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">
        <link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.css">
    </head>
    <body>
        <?php
        include './cabeceraAdmin.php';
        include '../modelos/tipos.php';

        //Paginacion
        $limit_end = 15;
        if (isset($_GET['pos'])) {
            $ini = $_GET['pos'];
        } else {
            $ini = 1;
        }
        $init = ($ini - 1) * $limit_end;
        $url = basename($_SERVER ["PHP_SELF"]);

        if (isset($_GET['action'])) {
            switch ($_GET['action']) {
                case 'eliminar':
                    echo "Dentro";
                    $eliminado = eliminarTipo($_GET['id']);
                    if ($eliminado) {
                        header('Location: tiposProductos.php');
                    } else {
                        ?>
                        <div class="alert alert-danger" role="alert">no se ha podido eliminar</div>
                        <?php
                    }

                    break;

                case 'editar':
                    header("Location: editTipo.php?id=" . $_GET['id'] . "");
                    break;
            }
        }
        ?>

        <!-- Tab panes -->
        <div class="pagina-producto">
            <div class="container">
                <div class="tab-content">
                    <div class="tab-pane active">
                        <br/>
                        <p><button type="button" class="btn btn-primary"  onClick="window.location.href = './insertTipo.php';"><span class="glyphicon glyphicon-plus"></span> Añadir</button></p>
                        <br/>
                        <div class="table-responsive">
                            <table id="example" cellspacing="0" width="100%" class="table table-hover">
                                <caption><h2>Tipos productos</h2></caption>	
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Tipo padre</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $tipos = consultarTipos();
                                    $total = ceil(count($tipos) / $limit_end);
                                    if (count($tipos) > 1) {
                                        foreach ($tipos as $tipo) {
                                            if (isset($tipo['idTipo'])) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $tipo['nombre'] ?></td>
                                                    <td><?php
                                                        if ($tipo['idPadre'] > 0) {
                                                            $padre = consultarPadre($tipo['idPadre']);
                                                            echo $padre[0]['nombre'];
                                                        } else {
                                                            echo "-";
                                                        }
                                                        ?></td>
                                                    <td>
                                                        <a href="?action=editar&id=<?php echo $tipo['idTipo']; ?>"><span class="glyphicon glyphicon-edit"></span> Editar</a>
                                                        <a href="?action=eliminar&id=<?php echo $tipo['idTipo']; ?>"><span class="glyphicon glyphicon-trash"></span> Borrar</a>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                    } else {
                                        ?>
                                    <div class="alert alert-warning" role="alert">No existen tipos de productos <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button></div>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                            <div>
                                <ul class="pagination">
                                    <?php
                                    if (($ini - 1) == 0) {
                                        ?>
                                        <li class="disabled"><a href="#">&laquo;</a></li>
                                            <?php
                                        } else {
                                            echo "<li><a href='$url?pos=" . ($ini - 1) . "'><b>&laquo;</b></a></li>";
                                        }
                                        for ($k = 1; $k <= $total; $k++) {
                                            if ($ini == $k) {
                                                echo "<li><a href='#'><b>" . $k . "</b></a></li>";
                                            } else {
                                                echo "<li><a href='$url?pos=$k'>" . $k . "</a></li>";
                                            }
                                        }
                                        if ($ini == $total) {
                                            echo "<li><a href='#'>&raquo;</a></li>";
                                        } else {
                                            echo "<li><a href='$url?pos=" . ($ini + 1) . "'><b>&raquo;</b></a></li>";
                                        }
                                        ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            include './pie.php';
            ?>
            <script src="../js/jquery-1.11.1.min.js"></script>
            <script src="../js/bootstrap.js"></script>
    </body>
</html>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_end_flush();
