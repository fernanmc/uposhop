<?php
session_start();
if($_SESSION["logueado"]==True){
    $idUser=$_SESSION["email"];
}else{
    session_abort();
    header("Location:loginAdmin.php");
}
?>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_start();
?>
<!DOCTYPE html>
<html lang="es">
    <head>

        <meta charset="UTF-8">
        <title>UPOShop</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">
         <link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.css">
        
    </head>
    <body>
        <?php
        include 'cabeceraAdmin.php';
        include '../modelos/proveedores.php';
        
        if (isset($_GET['id'])) {
            $idProveedor = $_GET['id'];
            $proveedor = consultarProveedor($idProveedor);
            $nombre = $proveedor['nombre'];
            $personaContacto = $proveedor['personaContacto'];
            $telefono = $proveedor['telefono'];
            $direccion = $proveedor['direccion'];
        }
        
        if (isset($_POST['guardar'])) {
            $filtros = Array(
                'nombre' => FILTER_SANITIZE_MAGIC_QUOTES,
                'personaContacto' => FILTER_SANITIZE_MAGIC_QUOTES,
                'telefono' => FILTER_SANITIZE_MAGIC_QUOTES,
                'direccion' => FILTER_SANITIZE_MAGIC_QUOTES
            );
            $result = filter_input_array(INPUT_POST, $filtros);
            
            $nombre = $result['nombre'];
            $personaContacto = $result['personaContacto'];
            $direccion = $result['direccion'];
            
            if ($result['telefono'] != "") {
                if (telefono_valido($result['telefono'])) {
                    $telefono = $result['telefono'];
                } else {
                    $errores[] = "El Teléfono no es correcto";
                }
            } else {
                $telefono = null;
            }
            if (!isset($errores)) {
                $insertado = editarProveedor($idProveedor, $nombre, $personaContacto, $telefono, $direccion);
                if ($insertado) {
                    header("Location:proveedores.php");
                } else {
                    $errores[] = "Ha habido un error";
                }
            }
        }
        ?>
        <div class="pagina-producto">
            <div class="container">
                <div class="tab-content">
                    <br/>
                    <?php
                    if (isset($errores)) {
                        ?>
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                <?php
                                foreach ($errores as $error) {
                                    ?>
                                    <li><?php echo $error; ?></li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                        <?php
                    }
                    ?>
                    <br/>
                    <h2>Editar Proveedor</h2>
                    <form role="form" method="POST" action="#">
                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control" id="nombre" name="nombre"  placeholder="Introduzca el nombre" value="<?php echo $nombre ?>">
                        </div>                     
                        <div class="form-group">
                            <label for="idPadre">Persona de contacto</label>
                            <input type="text" class="form-control" id="personaContacto" name="personaContacto"  placeholder="Introduzca la persona de contacto" value="<?php echo $personaContacto ?>">
                        </div>
                        <div class="form-group">
                            <label for="nombre">Teléfono</label>
                            <input type="text" class="form-control" id="telefono" name="telefono"  placeholder="Introduzca el teléfono" value="<?php echo $telefono ?>">
                        </div>
                        <div class="form-group">
                            <label for="nombre">Dirección</label>
                            <input type="text" class="form-control" id="direccion" name="direccion"  placeholder="Introduzca la dirección" value="<?php echo $direccion ?>">
                        </div>
                        <button type="submit" name="guardar" class="btn btn-primary">Guardar</button>
                        <button type="button" class="btn btn-success" onClick="window.location.href='index.php';" >Volver</button>
                    </form>
                    <br/>
                </div>
            </div>
        </div>
        <?php
        include 'pie.php';
        ?>
        <script src="../js/jquery-1.11.1.min.js"></script>
        <script src="../js/bootstrap.js"></script>
    </body>
</html>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_end_flush();

