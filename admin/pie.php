 <div class="pie">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="copyright">
                            <p>&copy; 2015 UPOshop. All Rights Reserved. Coded with <i class="fa fa-heart"></i> by <a href="#" target="_blank">Grupo06</a></p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="iconosTarjetas">
                            <i class="fa fa-cc-discover"></i>
                            <i class="fa fa-cc-mastercard"></i>
                            <i class="fa fa-cc-paypal"></i>
                            <i class="fa fa-cc-visa"></i>
                        </div>
                    </div>
                </div>
            </div>
</div>