<?php
session_start();
if ($_SESSION["logueado"] == True) {
    $idUser = $_SESSION["email"];
} else {
    session_abort();
    header("Location:loginAdmin.php");
}
?>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_start();
?>
<!DOCTYPE html>
<html lang="es">
    <head>

        <meta charset="UTF-8">
        <title>UPOShop</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">
        <link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.css">
        
    </head>
    <body>
        <?php
        include 'cabeceraAdmin.php';
        include '../modelos/productosOfertas.php';
        include '../modelos/tipos.php';
        include '../modelos/ofertas.php';

        if (isset($_GET['id'])) {
            $id = $_GET['id'];
        }
        if (isset($_POST['guardar'])) {
            $filtros = Array(
                'idTipo' => FILTER_SANITIZE_NUMBER_INT,
                'idOferta' => FILTER_SANITIZE_NUMBER_INT
            );
            $result = filter_input_array(INPUT_POST, $filtros);


            $idTipo = $result['idTipo'];
            $idOferta = $result['idOferta'];
            if (!isset($errores)) {
                $insertado = editarProductoOferta($id, $idTipo, $idOferta);
                if ($insertado) {
                    header("Location: productosOfertas.php");
                } else {
                    $errores[] = "Ha habido un error";
                }
            }
        }
        ?>
        <div class="pagina-producto">
            <div class="container">
                <div class="tab-content">
                    <br/>
                    <?php
                    if (isset($errores)) {
                        ?>
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                <?php
                                foreach ($errores as $error) {
                                    ?>
                                    <li><?php echo $error; ?></li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                        <?php
                    }
                    ?>
                    <br/>
                    <h2>Editar Productos con ofertas</h2>
                    <form role="form" method="POST" action="#">
                        <?php
                        $tipos = consultarTipos();
                        ?>
                        <div class="form-group">
                            <label for="idTipo">Tipo Producto</label>
                            <select class="form-control" id="idTipo" name="idTipo">
                                <option value="0" selected >Tipo producto</option>
                                <?php
                                $padres = consultarTiposPadres();
                                if (count($padres) > 1) {
                                    foreach ($padres as $padre) {
                                        if (isset($padre['idTipo'])) {
                                            ?>
                                            <optgroup  label="<?php echo $padre['nombre'] ?>"</optgroup> 
                                            <?php
                                            $hijos = consultarHijos($padre['idTipo']);
                                            if (count($hijos) > 1) {
                                                foreach ($hijos as $hijo) {
                                                    ?>
                                                    <option value = "<?php echo $hijo['idTipo'] ?>"><?php echo $hijo['nombre'] ?></option>
                                                    <?php
                                                }
                                            }
                                        }
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <?php
                        $ofertas = consultarOfertas();
                        ?>
                        <div class="form-group">
                            <label for="idOferta">Oferta</label>
                            <select class="form-control" id="idOferta" name="idOferta">
                                <option value="0" selected >Oferta</option>
                                <?php
                                if (count($ofertas) > 1) {
                                    foreach ($ofertas as $oferta) {
                                        if (isset($oferta['nombre'])) {
                                            ?>
                                            <option value="<?php echo $oferta['idOferta']; ?>" ><?php echo $oferta['nombre']; ?></option>      
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </select>
                        </div>

                        <button type="submit" name="guardar" class="btn btn-primary">Guardar</button>
                        <button type="button" class="btn btn-success" onClick="window.location.href = 'productosOfertas.php';" >Volver</button>
                    </form>
                    <br/>
                </div>
            </div>
        </div>
        <?php
        include 'pie.php';
        ?>
        <script src="../js/jquery-1.11.1.min.js"></script>
        <script src="../js/bootstrap.js"></script>
    </body>
</html>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_end_flush();


