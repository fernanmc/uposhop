<!DOCTYPE html>
<html lang="es">
<?php
session_start();
ob_start();
if(isset($_SESSION["logueado"])){
   $idUser=$_SESSION["email"];
}else{
    session_destroy();
     header("Location: login.php");
}
?>
    <head>
        <meta charset="UTF-8">
        <title>UPOShop</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.css">
    </head>
    <body>
       <?php      
        
       include './cabecera.php';
       include './modelos/productos.php'; 
       include './modelos/usuarios.php';
           
      //validamos los campos antes de continuar con la compra
       if (isset($_POST['continuar'])) {
            $filtros = Array(
                'nombre' => FILTER_SANITIZE_MAGIC_QUOTES,
                'apellidos' => FILTER_SANITIZE_MAGIC_QUOTES,
                'direccion' => FILTER_SANITIZE_MAGIC_QUOTES,
                'localidad' => FILTER_SANITIZE_MAGIC_QUOTES,
                'provincia' => FILTER_SANITIZE_MAGIC_QUOTES,
                'cp' => FILTER_SANITIZE_MAGIC_QUOTES,
                'telefono' => FILTER_SANITIZE_MAGIC_QUOTES,
            );
            //print_r($filtros);
            $result = filter_input_array(INPUT_POST, $filtros);
            
            if($result['nombre']!=""){
                $_SESSION["nombre"] = $result['nombre'];
               // $nombre=$result['nombre'];
            }else{
                $errores[]="El nombre es un campo obligatorio";
            }
            if($result['apellidos']!=""){
              $_SESSION["apellidos"] = $result['apellidos'];
              //$apellidos=$result['apellidos'];
            }else{
                $errores[]="Los apellidos es un campo obligatorio";
            }
            if($result['telefono']!=null){
               $_SESSION["telefono"] = $result['telefono'];
               //$telefono=$result['telefono'];
            }else{
                 $errores[]="El telefono es un campo obligatorio";
            }
            if($result['direccion']!=""){
                $_SESSION["direccion"] = $result['direccion'];
                //$direccion=$result['direccion'];
            }else{
                 $errores[]="La dirección es un campo obligatorio";
            }
            if($result['localidad']!="Seleccione antes una localidad"){
                $_SESSION["ciudad"] = $result['localidad'];
                //$ciudad=$result["localidad"];
            }else{
                  $errores[]="La ciudad es un campo obligatorio";
              } 
            if($result['cp']!=""){
                if(cp_valido($result['cp'])){
                   $_SESSION["cp"] = $result['cp'];
                }else{
                    $errores[]="El código postal no es correcto";
                  }
              }else{
                  $errores[]="El código postal es un campo obligatorio";
              }   
           
            
            if($result['telefono']!=""){
              if(telefono_valido( $result['telefono'])){
                 $_SESSION["telefono"] = $result['telefono'];
                 //$telefono=$result['telefono'];
              }else{
                  $errores[]="El Teléfono no es correcto";
              }   
            }
            
            if($result['provincia']!="Seleccione su provincia..."){
                $_SESSION["provincia"] = $result['provincia'];
                //$provincia=$result['provincia'];
            }
            //print_r($_SESSION);
            $posteado=True;
            if(!isset($errores)){
                header("Location: checkout.php");
            }
          }  
       ?>
        <div class="pagina-producto">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="producto-sidebar">
                        <h2 class="sidebar-titulo">Buscar Productos</h2>
                        <form method="POST" action="#">
                            <input type="text" placeholder="Buscar Productos..." name="busqueda" id="busqueda">
                            <input type="submit" value="Search" name="search">
                        </form>
                    </div>

                         <div class="producto-sidebar">
                        <h2 class="sidebar-titulo"> PRODUCTOS</h2>
                        <?php
                          $productosRelacionados= consultarProductosRecientes();
                          foreach ($productosRelacionados as $relacionado) {
                               if(isset($relacionado["idProducto"])){
                               $fotorelacionado=  listarFoto($relacionado['idProducto']);
                              
                        ?>
                        <div class="miniatura-reciente">
                            <img src="img/<?php echo $fotorelacionado[0];?>" class="miniatura" alt="">
                            <h2><a href="product.php?idProduct=<?php echo $relacionado['idProducto'] ?>"><?php echo $relacionado["nombre"];?></a></h2>
                            <div class="producto-sidebar-precio">
                                <ins><?php echo $relacionado["precio"]." €";?></ins>
                            </div>                             
                        </div>
                        <?php
                               }
                        }
                        ?>
                       
                    </div>

                        <div class="producto-sidebar">
                        <h2 class="sidebar-titulo">ÚLTIMOS PRODUCTOS</h2>
                        <ul>
                              <?php
                          $productosRecientes= consultarProductosRecientes();
                          foreach ($productosRecientes as $reciente) {
                              if(isset($reciente["idProducto"])){
                        ?>
                            <li><a href="product.php?idProduct=<?php echo $reciente['idProducto'] ?>"><?php echo  $reciente["nombre"]."-2016" ?></a></li>
                            
                              <?php
                              }
                        }
                        ?>
                        </ul>
                    </div>
                </div>
                  
                    <div class="col-md-6">
                    <div class="product-content-right">
                        <form method="post" action="">
                            <div class="form-group">
                              <?php
                                if (isset($errores)) {
                                    ?>
                                    <div class="alert alert-danger" role="alert">
                                        <ul>
                                            <?php
                                            foreach ($errores as $error) {
                                                ?>
                                                <li><?php echo $error; ?></li>
                                                <?php
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                    <?php
                                }
                                ?>
                                
                             <h2 class="sidebar-titulo">Formulario de Compra</h2>
                                    
                                    <?php 
                                  //  echo $nombre;
                                       if(!isset($posteado)){
                                            $usuario=consultarUser($idUser);
                                            $nombre=$usuario["nombre"];
                                            $apellidos=$usuario["apellidos"];
                                            $direccion=$usuario["direccion"];
                                            $telefono=$usuario["telefono"];
                                            $ciudad=$usuario["ciudad"];
                                            $provincia=$usuario["provincia"];
                                            $cp=$usuario["cp"];
                                            unset($posteado);
                                       }else{
                                            $nombre=$_SESSION["nombre"];
                                            $apellidos=$_SESSION["apellidos"];
                                            $direccion=$_SESSION["direccion"];
                                            $telefono=$_SESSION["telefono"];
                                            $ciudad=$_SESSION["ciudad"];
                                            $provincia=$_SESSION["provincia"];
                                            $cp=$_SESSION["cp"];
                                       }
                                        
                                        
                                    ?>
                                        <div class="form-group">
                                            <label>Nombre y Apellidos</label>
                                            <input class="form-control" type="text" name="nombre" id="nombre" placeholder="Nombre" value="<?php if(isset($nombre))echo $nombre ?>">
                                            <br>
                                            <input class="form-control" type="text" name="apellidos" id="apellidos" placeholder="Apellidos" value='<?php if(isset($apellidos)) echo $apellidos ?>'>
                                            <br>
                                            <input class="form-control" type="tel" minlength="9" maxlength="9" name="telefono" id="telefono" placeholder="NºTelefono" value="<?php if(isset($telefono))echo $telefono ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Datos de Envio </label>
                                            
                                            <br>
                                            <input type="text" class="form-control" id="direccion" name="direccion"  placeholder="Dirección de envio" value="<?php if(isset($direccion)) echo $direccion;?>">
                                            <br>
                                             <select class="form-control input-sm" name="provincia" id="provincia" onChange="return provinciaListOnChange()" value="<?php if(isset($provincia)) echo $provincia;?>">
                                                <option >Seleccione su provincia...</option>        
                                                <?php
                                                $xml = simplexml_load_file('provincias/provinciasypoblaciones.xml');
                                                $result = $xml->xpath("/lista/provincia/nombre | /lista/provincia/@id");

                                                for ($i = 0; $i < count($result); $i+=2) {
                                                    $e = $i + 1;
                                                    $resprovincia = $result[$e];
                                                    if($result[$i]==$usuario["provincia"]){
                                                    echo("<option value='$result[$i]' selected>$resprovincia</option>");

                                                         }else{
                                                    echo("<option value='$result[$i]'>$resprovincia</option>");
                                                        }
                                                    }
                                                ?>
                                            </select>
                                            <br>
                                            <select class="form-control input-sm" name="localidad" id="municipio" >
                                                    <option >Seleccione antes una localidad</option>
                                                </select> 
                                            <span id="advice"> </span>
                                            <br>
                                             <input type="text" minlength="5" minlength="5" class="form-control" id="cp" name="cp"  placeholder="Código Postal: 00000" value="<?php if(isset($cp)) echo $cp?>">
                                             <br>
                                             <input type="button"  value="Volver" onclick="window.location.href = './cart.php';" name="cancelar" >
                                              <input type="submit" value="Confirmar" name="continuar">
                                        </div>                                                
                                </div>                                              
                            </form>
                         <table cellspacing="0" class="tabla_carro cart">
                                  <thead>
                                        <tr>
                                            <th class="producto-miniatura">&nbsp;</th>
                                            <th class="producto-nombre">Producto</th>
                                            <th class="producto-precio">Precio</th>
                                            <th class="producto-cantidad">Cantidad</th>
                                            <th class="product-subtotal">Total</th>
                                        </tr>
                                    </thead>
                             <tbody>
                                        <?php
                                        $products="";
                                        if(isset($_COOKIE["carrito"])){
                                        foreach ($_COOKIE["carrito"] as $productoCarro) {
                                                                   
                                          //  var_dump($productoCarro)   
                                            $producto=  consultarProducto($productoCarro["id"]);
                                            $imagen= listarFoto($productoCarro["id"]);
                                            $productoCarro["cantidad"]=$_COOKIE["carrito"][$productoCarro["id"]]["cantidad"];
                                            $products.=$producto["nombre"]." Cant-".$productoCarro["cantidad"]." \n";
                                            //echo $products;
                                        ?>
                                        <tr>
                                            

                                            <td class="producto-miniatura">
                                                <a href="product.php?idProduct=<?php echo $producto["idProducto"]; ?>"><img width="145" height="145" alt="poster_1_up" class="shop_thumbnail" src="img/<?php echo $imagen[0] ?>"></a>
                                            </td>

                                            <td class="producto-nombre">
                                                <a href="product.php?idProduct=<?php echo $producto["idProducto"]; ?>"><?php echo $producto["nombre"]; ?></a> 
                                            </td>

                                            <td class="producto-precio">
                                                <span class="precio"><?php echo $producto["precio"]." €"; ?></span> 
                                            </td>

                                            <td class="producto-cantidad">
                                                <div class="cantidad">
                                                    <input type="number" readonly  class="cantidad"  title="Qty" value="<?php echo $productoCarro["cantidad"]; ?>" min="0" step="1">
                                                   
                                                </div>
                                            </td>

                                            <td class="product-subtotal">
                                                <span class="precio"><?php echo $productoCarro["cantidad"]*$productoCarro["precio"]." €"; ?></span> 
                                            </td>
                                        </tr>
                                         <?php
                                           }
                                         }
                                        ?>
                                        
                                       
                                    </tbody>
                                </table>
                             <div class="total_carro ">
                              <h2>Precio total</h2>

                                <table cellspacing="0">
                                    <tbody>
                                       
                                        <tr>
                                            <th>Order Total</th>
                                            <td><strong><span class="precio"><?php echo $preciototal." €"; ?></span></strong> </td>
                                        </tr>
                                    </tbody>
                                </table>
                             </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        <!--Pie de pagina footer-->
        <!--Fin del contenedor-->
        <?php
       include './pie.php';
       ?>
        <script src="./js/AjaxCode.js"></script>
        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.js"></script>

    </body>
</html>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_end_flush();