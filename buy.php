<!DOCTYPE html>
<html lang="es">
<?php
session_start();
ob_start();
if(isset($_SESSION["logueado"])){
   $idUser=$_SESSION["email"];
}else{
     session_destroy();
     header("Location: login.php");
}


?>
    <head>
        <meta charset="UTF-8">
        <title>UPOShop</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.css">
    </head>
    <body>
       <?php
      
      
       include './cabecera.php'; 
       include './modelos/productos.php';
       include './modelos/compras.php';
        
      if(isset($_GET["idCompra"])){
            $idCompra=$_GET["idCompra"];
            $compra=  consultarCompra($idCompra);
            $email=$idUser;
            $direccion="".$compra["direccion"]." /".$compra["ciudad"]." -CP: ".$compra["cp"];
            $precio=$compra["precioTotal"];
            $estado=$compra["estado"];
            $telefono=$compra["telefono"];
            $registro=$compra["fechaRegistro"];
            $modificacion=$compra["fechaActualizacion"];
        }
        if(isset($_POST["imprimir"])){
            header("Location: factura.php?idCompra=$idCompra");
        }

       ?>
        
        <div class="pagina-producto">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="producto-sidebar">
                        <h2 class="sidebar-titulo">Buscar Productos</h2>
                        <form method="POST" action="#">
                            <input type="text" placeholder="Buscar Productos..." name="busqueda" id="busqueda">
                            <input type="submit" value="Search" name="search">
                        </form>
                    </div>

                         <div class="producto-sidebar">
                        <h2 class="sidebar-titulo"> PRODUCTOS</h2>
                        <?php
                          $productosRelacionados= consultarProductosRecientes();
                          foreach ($productosRelacionados as $relacionado) {
                               if(isset($relacionado["idProducto"])){
                               $fotorelacionado=  listarFoto($relacionado['idProducto']);
                              
                        ?>
                        <div class="miniatura-reciente">
                            <img src="img/<?php echo $fotorelacionado[0];?>" class="miniatura" alt="">
                            <h2><a href="product.php?idProduct=<?php echo $relacionado['idProducto'] ?>"><?php echo $relacionado["nombre"];?></a></h2>
                            <div class="producto-sidebar-precio">
                                <ins><?php echo $relacionado["precio"]." €";?></ins>
                            </div>                             
                        </div>
                        <?php
                               }
                        }
                        ?>
                       
                    </div>

                        <div class="producto-sidebar">
                        <h2 class="sidebar-titulo">ÚLTIMOS PRODUCTOS</h2>
                        <ul>
                              <?php
                          $productosRecientes= consultarProductosRecientes();
                          foreach ($productosRecientes as $reciente) {
                              if(isset($reciente["idProducto"])){
                        ?>
                            <li><a href="product.php?idProduct=<?php echo $reciente['idProducto'] ?>"><?php echo  $reciente["nombre"]."-2016" ?></a></li>
                            
                              <?php
                              }
                        }
                        ?>
                        </ul>
                    </div>
                </div>
                  
                    <div class="col-md-8">
                    <div class="product-content-right">
                       <form method="post" action="">
                        <div class="form-group">
                              <h2>Datos Del Usuario</h2>

                                <table cellspacing="0" class="tabla_carro cart">
                                    <thead>
                                        <tr>
                                            <td>Email</td>
                                            <td>Dirección</td>
                                            <td>Telefono</td>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                       
                                        <tr>
                                            
                                            <td><?php echo $email ?> </td>
                                            <td><?php echo $direccion ?> </td>
                                            <td><?php echo $telefono ?> </td>
                                        </tr>
                                    </tbody>
                                </table>
                              
                                <table cellspacing="0" class="tabla_carro cart">
                                    <thead>
                                        <tr>
                                            <th class="producto-miniatura">&nbsp;</th>
                                            <th class="producto-nombre">Product</th>
                                            <th class="producto-precio">Precio</th>
                                            <th class="producto-cantidad">Cantidad</th>
                                            <th class="product-subtotal">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        //$ PRODUCTOS="";
                                        $productosComprados=  consultarProductosCompras($idCompra);
                                        if(count($productosComprados)){
                                        foreach ($productosComprados as $productoComprado) {
                                         if(isset($productoComprado["idProducto"])){                         
                                            
                                            $imagen= listarFoto($productoComprado["idProducto"]);
                                        ?>
                                        <tr>
                                            

                                            <td class="producto-miniatura">
                                                <a href="product.php?idProduct=<?php echo $productoComprado["idProducto"]; ?>"><img width="145" height="145" alt="poster_1_up" class="shop_thumbnail" src="img/<?php echo $imagen[0] ?>"></a>
                                            </td>

                                            <td class="producto-nombre">
                                                <a href="product.php?idProduct=<?php echo $productoComprado["idProducto"]; ?>"><?php echo $productoComprado["nombre"]; ?></a> 
                                            </td>

                                            <td class="producto-precio">
                                                <span class="precio"><?php echo $productoComprado["precio"]." €"; ?></span> 
                                            </td>

                                            <td class="producto-cantidad">
                                                <div class="cantidad">
                                                    <input type="number" readonly  class="cantidad"  title="Qty" value="<?php echo $productoComprado["cantidad"]; ?>" min="0" step="1">
                                                   
                                                </div>
                                            </td>

                                            <td class="product-subtotal">
                                                <span class="precio"><?php echo $productoComprado["cantidad"]*$productoComprado["precio"]." €"; ?></span> 
                                            </td>
                                        </tr>
                                         <?php
                                                }
                                           }
                                         }
                                        ?>
                                        <tr>
                                            <td class="acciones" colspan="6">
                                                
                                                <input type="button"  value="Volver" onclick="window.location.href = './checkouts.php';" name="Volver" >
                                                <input type="submit" value="Descargar PDF" name="imprimir">
                                            </td>
                                        </tr>
                                       
                                    </tbody>
                                </table>
                            </div>
                            
                            </form>
                            <div class="total_carro ">
                              <h2>Precio total</h2>

                                <table cellspacing="0">
                                    <tbody>
                                       
                                        <tr>
                                            <th>Order Total</th>
                                            <td><strong><span class="precio"><?php echo $compra["precioTotal"]." €"; ?></span></strong> </td>
                                        </tr>
                                         <tr>
                                            <th>Fecha Compra</th>
                                            <td><span class="precio"><?php echo $compra["fechaRegistro"]; ?></strong> </td>
                                        </tr>
                                        <tr>
                                            <th>Fecha Última Actualización</th>
                                            <td><span class="precio"><?php echo $compra["fechaActualizacion"]; ?></strong> </td>
                                        </tr>
                                        <tr>
                                            <th>Modificación</th>
                                            <td><strong><span class="precio"><?php echo $estado; ?></span></strong> </td>
                                        </tr>
                                    </tbody>
                                </table>
                             </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
        <!--Pie de pagina footer-->
        <!--Fin del contenedor-->
        <?php
       include './pie.php';
       ?>
        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.js"></script>

    </body>
</html>
<?php
//Para poder poder las cabeceras en cualquier lugar del codigo
ob_end_flush();