<!DOCTYPE html>
<?php
ob_start();
session_start();
//include './cabecera.php';
include './modelos/usuarios.php';

if (isset($_POST['envio'])) {
    $filtros = Array(
                'inputEmail' => FILTER_SANITIZE_MAGIC_QUOTES,
                'inputPassword' => FILTER_SANITIZE_MAGIC_QUOTES,
                'repeatPassword'=>FILTER_SANITIZE_MAGIC_QUOTES,
            );
            $result = filter_input_array(INPUT_POST, $filtros);
            if (!is_numeric($result['inputEmail']) && $result['inputEmail'] != "") {
                $email = $result['inputEmail'];
            } else {
                $errores[] = "No ha introducido el email";
            }
        if ((!is_numeric($result['inputPassword']) && $result['inputPassword'] != "") && (!is_numeric($result['repeatPassword']) && $result['repeatPassword'] != "")) {
        $password = $result['inputPassword'];
        $repeatPassword = $result['repeatPassword'];
        if ($password == $repeatPassword) {      //  Contraseñas coinciden...
            if (comprobarEmail($email) == true) {    //  Email no existe en el sistema...
                $password = password_hash($password, PASSWORD_BCRYPT, ['cost' => 12]);
                $registrado = registrarUser($email, $password);
            } else {
                $errores[] = "El mail ya existe";
            }
        } else {
            $errores[] = "El password no coincide";
        }
    } else {
        $errores[] = "El password debe de ser completo";
    }
    // $email = $_POST['inputEmail'];
    //$password = $_POST['inputPassword'];
   // $repeatPassword = $_POST['repeatPassword'];

    //echo "$password";
   // echo "$repeatPassword";
    
    if(isset($registrado)){
        if($registrado==True){
            $_SESSION["logueado"]=True;
            $_SESSION["email"]=$email;
            header("Location: index.php");
        }
    }
}
?>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>UPOshop</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.css">
    </head>
    <body>
        <?php
        include './cabecera.php';
        ?>
        <div class="pagina-producto">
            <div class="container">
                <div class="row">
                    
                     <?php
                    if (isset($errores)) {
                        ?>
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                <?php
                                foreach ($errores as $error) {
                                    ?>
                                    <li><?php echo $error; ?></li>
                                    <?php
                                }
                                unset($errores);
                                ?>
                            </ul>
                    <?php } ?>
                        </div>
                    <form class="login" action="#" method="POST">
                        
                        <h2 class="login-heading">Please sign in</h2>

                        <label for="inputEmail" class="sr-only">Email address</label>
                        <input type="email" id="inputEmail" name="inputEmail" class="form-control" placeholder="Email address" required autofocus onchange="checkEmail(this)">
                        <label id="emailError" name="emailError" value=""></label>

                        <label for="inputPassword" class="sr-only">Password</label>
                        <input type="password" id="inputPassword" name="inputPassword" class="form-control" placeholder="Password" data-indicator="pwindicator" required onkeypress="fortaleza()" onchange="checkPassword(this)">
                        <label id="passwordError" name="emailError" value=""></label>
                        <meter id="progressbar" value="0" max="100"></meter>

                        <label for="repeatPassword" class="sr-only">Password</label>
                        <input type="password" id="repeatPassword" name="repeatPassword" class="form-control" placeholder="Password" required onchange="checkConfirmPassword(this)">
                        <label id="repeatPasswordError" name="emailError"></label>

                        <button type="submit" class="btn btn-lg btn-primary btn-block" name="envio" >Sign in</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="pie">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="copyright">
                            <p>&copy; 2015 UPOshop. All Rights Reserved. Coded with <i class="fa fa-heart"></i> by <a href="#" target="_blank">Grupo06</a></p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="iconosTarjetas">
                            <i class="fa fa-cc-discover"></i>
                            <i class="fa fa-cc-mastercard"></i>
                            <i class="fa fa-cc-paypal"></i>
                            <i class="fa fa-cc-visa"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery.complexify.js"></script>
        <script src="js/validarRegister.js"></script>

    </body>
</html>
<?php
ob_flush();